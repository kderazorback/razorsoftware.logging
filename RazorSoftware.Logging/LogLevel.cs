﻿/*
    Copyright © 2018 RazorSoftware
    Fabian R. kderazorback@me.com
    This file is part of RazorSoftware Logging Library.

    RazorSoftware Logging Library is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RazorSoftware Logging Library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RazorSoftware Logging Library.  If not, see <http://www.gnu.org/licenses/>.
 */


namespace com.RazorSoftware.Logging
{
    /// <summary>
    /// Stores values for different LogLevels supported by the Message Logger.
    /// </summary>
    /// <remarks>
    /// A custom int16 value can be specified for LogLevel, to allow support for custom Messages codes in your application.
    /// <para>And the members on the Logging namespace will process them according to the section where they belongs.</para>
    /// <para>Values ranging from zero to 0x0FFF are treated as Debug messsages. (4.096 entries)</para>
    /// <para>Values ranging from 0x1000 to 0x2FFF are treated as Information Messages. (8.192 entries)</para>
    /// <para>Values ranging from 0x3000 to 0x4FFF are treated as Warnings. (8.192 entries)</para>
    /// <para>Values ranging from 0x5000 to 0x6FFF are treated as Errors. (8.192 entries)</para>
    /// <para>A value of 0xFFFF (-1) is reserved for Crash messages.</para>
    /// </remarks>
#if RAZORSOFTWARE_PUBLIC_API
    public
#else
    internal
#endif
    enum LogLevel : ushort
    {
        /// <summary>
        /// Detailed messages for Debugging purposes
        /// </summary>
        Debug = 0x0000,
        /// <summary>
        /// Normal Information messages
        /// </summary>
        Message = 0x1000,
        /// <summary>
        /// General Application Warnings
        /// </summary>
        Warning = 0x3000,
        /// <summary>
        /// General Application Errors
        /// </summary>
        Error = 0x5000,
        /// <summary>
        /// Reserved for Sever errors that terminates the application
        /// </summary>
        Crash = 0xFFFF,
    }
}
