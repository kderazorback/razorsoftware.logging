﻿using com.RazorSoftware.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace RazorSoftware.Logging.Tests
{
    [TestClass]
    public class LogTests
    {
        [TestMethod]
        public void ExpandStrTokens_SamplePattern_MatchesExpectedResult()
        {
            Assert.AreEqual("%Example token string.", Log.ExpandStrTokens("%%Example token %2%@", ".", "string"));
        }

        [TestMethod]
        public void ExpandStrTokens_SamplePattern2_MatchesExpectedResult()
        {
            Assert.AreEqual("%!%%OK%%", Log.ExpandStrTokens("%% %5 %% %% %@ %% %%".Replace(" ", ""), "OK", "NO", "BAD", "BAD", "!", "ERROR"));
        }

        [TestMethod]
        public void ExpandStrTokens_EmptyParamsArray_ShouldReturnOriginalString()
        {
            string entry = "%% %4 %% %% %@ %% %%".Replace(" ", "");
            Assert.AreEqual(entry, Log.ExpandStrTokens(entry));
        }

        [TestMethod]
        public void ExpandStrTokens_SamplePattern4_MatchesExpectedResult()
        {
            Assert.AreEqual("%%!%x%%@%%", Log.ExpandStrTokens("%% %! %x %% %%@ %% %%".Replace(" ", ""), "BAD"));
        }

        [TestMethod]
        public void ExpandStrTokens_IndexedTokens_MatchesExpectedResult()
        {
            Assert.AreEqual("Item1 Item2 Item3 Item5 Item4", Log.ExpandStrTokens("%1 %2 %3 %5 %4", "Item1", "Item2", "Item3", "Item4", "Item5"));
        }

        [TestMethod]
        public void ExpandStrTokens_NonIndexedTokens_MatchesExpectedResult()
        {
            Assert.AreEqual("Item1 Item2 Item3 Item4 Item5", Log.ExpandStrTokens("%@ %@ %@ %@ %@", "Item1", "Item2", "Item3", "Item4", "Item5"));
        }

        [TestMethod]
        public void ExpandStrTokens_MixedTokens_MatchesExpectedResult()
        {
            Assert.AreEqual("Item1 Item2 Item5 Item3 Item4", Log.ExpandStrTokens("%@ %@ %5 %@ %@", "Item1", "Item2", "Item3", "Item4", "Item5", "Item6", "Item7"));
        }

        [TestMethod]
        public void ExpandStrTokens_EmptyString_ReturnsEmpty()
        {
            Assert.AreEqual("", Log.ExpandStrTokens("", "Item1", "Item2", "Item3", "Item4", "Item5"));
        }

        [TestMethod]
        public void ExpandStrTokens_MixedTokens_MatchesExpectedResult2()
        {
            Assert.AreEqual("Item1 Item1 Item1 Item1", Log.ExpandStrTokens("%1 %1 %1 %1", "Item1"));
        }
    }
}
