﻿/*
    Copyright © 2018 RazorSoftware
    Fabian R. kderazorback@me.com
    This file is part of RazorSoftware Logging Library.

    RazorSoftware Logging Library is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RazorSoftware Logging Library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RazorSoftware Logging Library.  If not, see <http://www.gnu.org/licenses/>.
 */


using System;

namespace com.RazorSoftware.Logging.Outputs
{
    /// <summary>
    /// Allows an Attached Debugger to be used as Output for entries sent to the <see cref="Log"/> Class.
    /// </summary>
    /// <remarks>
    /// If no debugger is attached, all entries will be discarded.
    /// </remarks>
    [Serializable]
#if RAZORSOFTWARE_PUBLIC_API
    public class DebuggerOutput : LogOutput
#else
    internal class DebuggerOutput : LogOutput
#endif
    {
        // Methods
        /// <summary>
        /// Writes the specified entry to the Debugger.
        /// </summary>
        /// <param name="entry">Text that will be displayed on the Debugger</param>
        /// <remarks>
        /// This method DONT write an end of line to the output.
        /// </remarks>
        public override void Write(string entry)
        {
            if (Enabled)
                System.Diagnostics.Debugger.Log(0, "", entry);
        }

        /// <summary>
        /// Writes the specified entry to the Debugger.
        /// </summary>
        /// <param name="entry">Text that will be displayed on the Debugger</param>
        /// <remarks>
        /// This method DO write an end of line to the output.
        /// </remarks>
        public override void WriteLine(string entry)
        {
            if (Enabled)
                System.Diagnostics.Debugger.Log(0, "", entry + "\n");
        }

        /// <summary>
        /// Writes an end of line to the output.
        /// </summary>
        public void WriteLine()
        {
            if (Enabled)
                System.Diagnostics.Debugger.Log(0, "", "\n");
        }
    }
}
