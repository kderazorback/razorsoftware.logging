﻿/*
    Copyright © 2018 RazorSoftware
    Fabian R. kderazorback@me.com
    This file is part of RazorSoftware Logging Library.

    RazorSoftware Logging Library is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RazorSoftware Logging Library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RazorSoftware Logging Library.  If not, see <http://www.gnu.org/licenses/>.
 */


using System;
using System.Globalization;
using System.Reflection;
using System.Text;

namespace com.RazorSoftware.Logging
{
    /// <summary>
    /// Logs messages generated from an specific part of the Application to the central <see cref="Log"/> class.
    /// </summary>
    /// <remarks>
    /// This class automatically formats and categorizes logged entries.
    /// <para>The underlying <see cref="Log"/> class is Thread-Safe but this class and all its instances are NOT Thread-Safe.</para>
    /// </remarks>
#if RAZORSOFTWARE_PUBLIC_API
    public
#else
    internal
#endif
    class Logger : MarshalByRefObject, IDisposable
    {
        // IVars
        private string _lastEntry;
        private int _lastEntryRepeat;

        // Attributes
        /// <summary>
        /// Used to set the indentation of logged entries.
        /// </summary>
        /// <remarks>
        /// For each counter on this attribute, a prefix of three whitespaces will be added to each entry sent to the <see cref="Log"/>.
        /// </remarks>
        public int IndentLevel { get; private set; }

        /// <summary>
        /// If enabled, multiple messages with the same body and log level will be omitted from the output and instead, a counter will be added with the amount of times the item repeats.
        /// </summary>
        /// <remarks>
        /// This counter appears when a different entry is added to the <see cref="Log"/>.
        /// </remarks>
        public bool WrapRepetitions { get; set; }

        /// <summary>
        /// Stores the LogLevel of the last entry that was sent to this instance.
        /// </summary>
        /// <remarks>
        /// This is used only for the method <see cref="WriteLine()"/> to prevent adding blank lines for inactive entries (higher LogLevels than the specified).
        /// </remarks>
        public LogLevel LastLogLevel { get; private set; }

        /// <summary>
        /// Stores the amount of error messages that where sent to this instance since its creation.
        /// </summary>
        /// <remarks>This attribute is Arithmetically unchecked for overflows.</remarks>
        public int Errors { get; private set; }

        /// <summary>
        /// Stores the amount of warning messages that where sent to this instance since its creation.
        /// </summary>
        /// <remarks>This attribute is Arithmetically unchecked for overflows.</remarks>
        public int Warnings { get; private set; }

        /// <summary>
        /// Stores the amount of entries that where sent to this instance since its creation.
        /// </summary>
        /// <remarks>This attribute is Arithmetically unchecked for overflows.</remarks>
        public int Entries { get; private set; }

        /// <summary>
        /// Stores the name of the category assigned for all messages logged by this instance.
        /// </summary>
        /// <remarks>
        /// This name is prefixed to every entry logged by this instance.
        /// <para>If a null or empty string is specified, no category prefix will be added to the entries.</para>
        /// </remarks>
        public string CategoryName { get; set; }

        /// <summary>
        /// Stores the <see cref="LogProxy"/> that this instance will use for accessing the static <see cref="Log"/> class.
        /// </summary>
        /// <remarks>
        /// This attribute is for remoting purposes only. And allows a local instance of this class to send messages to a remote <see cref="Log"/> class in a separate <see cref="AppDomain"/>.
        /// <para>A new Proxy referencing a local <see cref="Log"/> class is automatically created in the constructor of this class. That means, this class operates by default on the local <see cref="Log"/> class in the current AppDomain.</para>
        /// </remarks>
        public LogProxy Proxy { get; set; }

        // Constructors
        /// <summary>
        /// Creates a new instance of the Logger class by specifying its Category name.
        /// </summary>
        /// <param name="category">Category name for all messages logged through the new class.</param>
        /// <remarks>
        /// This name is prefixed to every entry logged by this instance.
        /// <para>If a null or empty string is specified, no category prefix will be added to the entries.</para>
        /// </remarks>
        public Logger(string category)
        {
            CategoryName = category;
            Errors = 0;
            Warnings = 0;
            Entries = 0;
            _lastEntry = null;
            _lastEntryRepeat = 0;
            LastLogLevel = LogLevel.Message;
            Proxy = new LogProxy();
        }

        // Methods
        /// <summary>
        /// Writes the specified entry to the <see cref="Log"/> with an specified <see cref="LogLevel"/>.
        /// </summary>
        /// <exception cref="ArgumentException">Thrown when <see cref="Object.ToString()"/> cannot be called for an item inside the elements parameter.</exception>
        /// <param name="entry">Entry that will be logged.</param>
        /// <param name="level"><see cref="LogLevel"/> for the current <paramref name="entry"/>.</param>
        /// <param name="elements">Items that replace the tokens found on the <paramref name="entry"/>.</param>
        /// <remarks>
        /// This method DONT write an End Of Line to the outputs.
        /// <para>The <paramref name="elements"/> array replaces a list of tokens on the <paramref name="entry"/> string.</para>
        /// <para>These tokens are in the form of: %@ for index-less parameters and %n for indexed parameters where N is a int value ranging from 0 to 9 inclusive.</para>
        ///	<para>To parse each element, the method will call <see cref="Object.ToString()"/> method for the element, If the call fails, an exception is thrown.</para>
        ///	<para>To write the % symbol into the final entry, use %%.</para>
        /// </remarks>
        ///	<example>
        ///	This example shows how to use the tokens on the <paramref name="entry"/> parameter to print the values of the items on the <paramref name="elements"/> array
        ///	<code>
        ///	Write("Item with name %@ costs %@US$. Remember that its name is: %1", LogLevel.Message, "Red Sofa", 79.99f);
        ///	
        /// // This code prints:
        /// //   Item with name Red Sofa costs 79.99US$. Remember that its name is: Red Sofa
        /// </code>
        ///	</example>
        public void Write(string entry, LogLevel level, params object[] elements)
        {
            string expandedString = Log.ExpandStrTokens(entry, elements);
            Write(expandedString, level);
        }

        /// <summary>
        /// Writes the specified entry to the <see cref="Log"/> with an specified <see cref="LogLevel"/>.
        /// </summary>
        /// <param name="entry">Entry that will be logged.</param>
        /// <param name="level"><see cref="LogLevel"/> for the current entry.</param>
        /// <remarks>
        /// This method DONT write an End Of Line to the outputs.
        /// </remarks>
        public void Write(string entry, LogLevel level)
        {
            // Default Write() overload
            unchecked
            {
                Entries++;
                if (level > LogLevel.Error)
                    Errors++;
                else
                {
                    if (level > LogLevel.Warning)
                        Warnings++;
                }
            }
            
            Proxy.Write(BuildMessageString(entry, level), level);
        }

        /// <summary>
        /// Writes the specified entry to the <see cref="Log"/> with a <see cref="LogLevel.Message"/>.
        /// </summary>
        /// <param name="entry">Entry that will be logged.</param>
        /// <remarks>
        /// This method DONT write an End Of Line to the outputs.
        /// </remarks>
        public void Write(string entry)
        {           
            Write(BuildMessageString(entry, LogLevel.Message), LogLevel.Message);
        }

        /// <summary>
        /// Writes the specified entry into all outputs with a higher or equal <see cref="LogLevel"/> using the specified Background and Font colors. This variant replaces tokens in the form of %@ or %i (one-based) with the arguments provided in <paramref name="elements"/>>.
        /// </summary>
        /// <exception cref="ArgumentException">Thrown when <see cref="Object.ToString()"/> cannot be called for an item inside the elements parameter.</exception>
        /// <exception cref="IOException">Thrown when the entry cannot be written to an Output.</exception>
        /// <param name="entry">Entry that will be logged.</param>
        /// <param name="background">Background color used to display the message</param>
        /// <param name="foreground">Font color used to display the message</param>
        /// <param name="level"><see cref="LogLevel"/> for the current <paramref name="entry"/>.</param>
        /// <param name="elements">Items that replace the tokens found on the <paramref name="entry"/>.</param>
        /// <remarks>
        /// This method DONT write an End Of Line to the outputs.
        /// <para>The <paramref name="elements"/> array replaces a list of tokens on the <paramref name="entry"/> string.</para>
        /// <para>These tokens are in the form of: %@ for index-less parameters and %n for indexed parameters where N is a int value.</para>
        ///	<para>To parse each element, the method will call <see cref="Object.ToString()"/> method for the element, If the call fails, an exception is thrown.</para>
        ///	<para>To write the % symbol into the final entry, use %%.</para>
        /// </remarks>
        ///	<example>
        ///	This example shows how to use the tokens on the <paramref name="entry"/> parameter to print the values of the items on the <paramref name="elements"/> array
        ///	<code>
        ///	Write("Item with name %@ costs %@US$. Remember that its name is: %1", LogLevel.Message, "Red Sofa", 79.99f);
        ///	
        /// // This code prints:
        /// //   Item with name Red Sofa costs 79.99US$. Remember that its name is: Red Sofa
        /// </code>
        ///	</example>
        public void WriteColored(string entry, ConsoleColor background, ConsoleColor foreground, LogLevel level, params object[] elements)
        {
            unchecked
            {
                Entries++;
                if (level > LogLevel.Error)
                    Errors++;
                else
                {
                    if (level > LogLevel.Warning)
                        Warnings++;
                }
            }

            Proxy.WriteColored(BuildMessageString(entry, level), background, foreground, level, elements);
        }

        /// <summary>
        /// Writes the specified entry into all outputs with a higher or equal <see cref="LogLevel"/> using the specified Font color. This variant replaces tokens in the form of %@ or %i (one-based) with the arguments provided in <paramref name="elements"/>>.
        /// </summary>
        /// <exception cref="ArgumentException">Thrown when <see cref="Object.ToString()"/> cannot be called for an item inside the elements parameter.</exception>
        /// <exception cref="IOException">Thrown when the entry cannot be written to an Output.</exception>
        /// <param name="entry">Entry that will be logged.</param>
        /// <param name="foreground">Font color used to display the message</param>
        /// <param name="level"><see cref="LogLevel"/> for the current <paramref name="entry"/>.</param>
        /// <param name="elements">Items that replace the tokens found on the <paramref name="entry"/>.</param>
        /// <remarks>
        /// This method DONT write an End Of Line to the outputs.
        /// <para>The <paramref name="elements"/> array replaces a list of tokens on the <paramref name="entry"/> string.</para>
        /// <para>These tokens are in the form of: %@ for index-less parameters and %n for indexed parameters where N is a int value.</para>
        ///	<para>To parse each element, the method will call <see cref="Object.ToString()"/> method for the element, If the call fails, an exception is thrown.</para>
        ///	<para>To write the % symbol into the final entry, use %%.</para>
        /// </remarks>
        ///	<example>
        ///	This example shows how to use the tokens on the <paramref name="entry"/> parameter to print the values of the items on the <paramref name="elements"/> array
        ///	<code>
        ///	Write("Item with name %@ costs %@US$. Remember that its name is: %1", LogLevel.Message, "Red Sofa", 79.99f);
        ///	
        /// // This code prints:
        /// //   Item with name Red Sofa costs 79.99US$. Remember that its name is: Red Sofa
        /// </code>
        ///	</example>
        public void WriteColored(string entry, ConsoleColor foreground, LogLevel level, params object[] elements)
        {
            unchecked
            {
                Entries++;
                if (level > LogLevel.Error)
                    Errors++;
                else
                {
                    if (level > LogLevel.Warning)
                        Warnings++;
                }
            }

            Proxy.WriteColored(BuildMessageString(entry, level), foreground, level, elements);
        }

        /// <summary>
        /// Writes the specified entry into all outputs with a higher or equal <see cref="LogLevel"/> with the specified Background and Font colors.
        /// </summary>
        /// <exception cref="IOException">Thrown when the entry cannot be written to an Output.</exception>
        /// <param name="entry">Entry that will be logged.</param>
        /// <param name="background">Background color used to display the message</param>
        /// <param name="foreground">Font color used to display the message</param>
        /// <param name="level"><see cref="LogLevel"/> for the current entry.</param>
        /// <remarks>
        /// This method DONT write an End Of Line to the outputs.
        /// </remarks>
        public void WriteColored(string entry, ConsoleColor background, ConsoleColor foreground, LogLevel level = LogLevel.Message)
        {
            WriteColored(entry, background, foreground, level, null);
        }

        /// <summary>
        /// Writes the specified entry into all outputs with a higher or equal <see cref="LogLevel"/> with the specified Font color.
        /// </summary>
        /// <exception cref="IOException">Thrown when the entry cannot be written to an Output.</exception>
        /// <param name="entry">Entry that will be logged.</param>
        /// <param name="foreground">Font color used to display the message</param>
        /// <param name="level"><see cref="LogLevel"/> for the current entry.</param>
        /// <remarks>
        /// This method DONT write an End Of Line to the outputs.
        /// </remarks>
        public void WriteColored(string entry, ConsoleColor foreground, LogLevel level = LogLevel.Message)
        {
            WriteColored(entry, foreground, level, null);
        }

        /// <summary>
        /// Writes the specified entry to the <see cref="Log"/> with an specified <see cref="LogLevel"/>.
        /// </summary>
        /// <exception cref="ArgumentException">Thrown when <see cref="Object.ToString()"/> cannot be called for an item inside the elements parameter.</exception>
        /// <param name="line">Entry that will be logged.</param>
        /// <param name="level"><see cref="LogLevel"/> for the current <paramref name="line"/>.</param>
        /// <param name="elements">Items that replace the tokens found on the <paramref name="line"/>.</param>
        /// <remarks>
        /// This method DO write an End Of Line to the outputs.
        /// <para>The <paramref name="elements"/> array replaces a list of tokens on the <paramref name="line"/> string.</para>
        /// <para>These tokens are in the form of: %@ for index-less parameters and %n for indexed parameters where N is a int value ranging from 0 to 9 inclusive.</para>
        ///	<para>To parse each element, the method will call <see cref="Object.ToString()"/> method for the element, If the call fails, an exception is thrown.</para>
        ///	<para>To write the % symbol into the final entry, use %%.</para>
        /// </remarks>
        ///	<example>
        ///	This example shows how to use the tokens on the <paramref name="line"/> parameter to print the values of the items on the <paramref name="elements"/> array
        ///	<code>
        ///	Write("Item with name %@ costs %@US$. Remember that its name is: %1", LogLevel.Message, "Red Sofa", 79.99f);
        ///	
        /// // This code prints:
        /// //   Item with name Red Sofa costs 79.99US$. Remember that its name is: Red Sofa
        /// </code>
        ///	</example>
        public void WriteLine(string line, LogLevel level, params object[] elements)
        {

            string expandedString = Log.ExpandStrTokens(line, elements);
            WriteLine(expandedString, level);
        }

        /// <summary>
        /// Writes the specified entry to the <see cref="Log"/> with an specified <see cref="LogLevel"/>.
        /// </summary>
        /// <param name="line">Entry that will be logged.</param>
        /// <param name="level"><see cref="LogLevel"/> for the current entry.</param>
        /// <remarks>
        /// This method DO write an End Of Line to the outputs.
        /// </remarks>
        public void WriteLine(string line, LogLevel level)
        {
            // Default WriteLine() overload
            unchecked
            {
                Entries++;
                if (level > LogLevel.Error)
                    Errors++;
                else
                {
                    if (level > LogLevel.Warning)
                        Warnings++;
                }
            }

            // If the entry is the same as the previous one (including LogLevel)
            if (String.Equals(line, _lastEntry, StringComparison.OrdinalIgnoreCase) && level == LastLogLevel && !String.IsNullOrEmpty(line) && WrapRepetitions)
            {
                // Increase counter and ignore the entry
                _lastEntryRepeat++;
                return;
            }

            // The strings are not equal or are empty (both)

            if (!String.IsNullOrEmpty(_lastEntry) && _lastEntryRepeat > 0)
                Proxy.Write(BuildMessageString("     The last entry repeats " + _lastEntryRepeat.ToString("N0", CultureInfo.CurrentCulture) + " times.\n", LastLogLevel));

            _lastEntryRepeat = 0;
            _lastEntry = line;
            LastLogLevel = level;

            Proxy.WriteLine(BuildMessageString(line, level), level);
        }

        /// <summary>
        /// Writes the specified entry to the <see cref="Log"/> with a <see cref="LogLevel.Message"/>.
        /// </summary>
        /// <param name="line">Entry that will be logged.</param>
        /// <remarks>
        /// This method DO write an End Of Line to the outputs.
        /// </remarks>
        public void WriteLine(string line)
        {
            WriteLine(line, LogLevel.Message);
        }

        /// <summary>
        /// Writes a new blank line entry to the <see cref="Log"/> with an specified <see cref="LogLevel"/>.
        /// </summary>
        /// <param name="level"><see cref="LogLevel"/> for the new blank line.</param>
        public void WriteLine(LogLevel level)
        {
            Proxy.WriteLine(level);
        }

        /// <summary>
        /// Writes a new blank line entry to the <see cref="Log"/> with the same <see cref="LogLevel"/> as the last entry sent to this instance.
        /// </summary>
        public void WriteLine()
        {
            Proxy.WriteLine(LastLogLevel);
        }

        /// <summary>
        /// Writes the specified entry into all outputs with a higher or equal <see cref="LogLevel"/> using the specified Background and Font colors. This variant replaces tokens in the form of %@ or %i (one-based) with the arguments provided in <paramref name="elements"/>>.
        /// </summary>
        /// <exception cref="ArgumentException">Thrown when <see cref="Object.ToString()"/> cannot be called for an item inside the elements parameter.</exception>
        /// <exception cref="IOException">Thrown when the entry cannot be written to an Output.</exception>
        /// <param name="line">Entry that will be logged.</param>
        /// <param name="background">Background color used to display the message</param>
        /// <param name="foreground">Font color used to display the message</param>
        /// <param name="level"><see cref="LogLevel"/> for the current <paramref name="line"/>.</param>
        /// <param name="elements">Items that replace the tokens found on the <paramref name="line"/>.</param>
        /// <remarks>
        /// This method DO write an End Of Line to the outputs.
        /// <para>The <paramref name="elements"/> array replaces a list of tokens on the <paramref name="line"/> string.</para>
        /// <para>These tokens are in the form of: %@ for index-less parameters and %n for indexed parameters where N is a int value.</para>
        ///	<para>To parse each element, the method will call <see cref="Object.ToString()"/> method for the element, If the call fails, an exception is thrown.</para>
        ///	<para>To write the % symbol into the final entry, use %%.</para>
        /// </remarks>
        ///	<example>
        ///	This example shows how to use the tokens on the <paramref name="line"/> parameter to print the values of the items on the <paramref name="elements"/> array
        ///	<code>
        ///	Write("Item with name %@ costs %@US$. Remember that its name is: %1", LogLevel.Message, "Red Sofa", 79.99f);
        ///	
        /// // This code prints:
        /// //   Item with name Red Sofa costs 79.99US$. Remember that its name is: Red Sofa
        /// </code>
        ///	</example>
        public void WriteColoredLine(string line, ConsoleColor background, ConsoleColor foreground, LogLevel level, params object[] elements)
        {
            unchecked
            {
                Entries++;
                if (level > LogLevel.Error)
                    Errors++;
                else
                {
                    if (level > LogLevel.Warning)
                        Warnings++;
                }
            }

            Proxy.WriteColoredLine(BuildMessageString(line, level), background, foreground, level, elements);
        }

        /// <summary>
        /// Writes the specified entry into all outputs with a higher or equal <see cref="LogLevel"/> using the specified Font color. This variant replaces tokens in the form of %@ or %i (one-based) with the arguments provided in <paramref name="elements"/>>.
        /// </summary>
        /// <exception cref="ArgumentException">Thrown when <see cref="Object.ToString()"/> cannot be called for an item inside the elements parameter.</exception>
        /// <exception cref="IOException">Thrown when the entry cannot be written to an Output.</exception>
        /// <param name="line">Entry that will be logged.</param>
        /// <param name="foreground">Font color used to display the message</param>
        /// <param name="level"><see cref="LogLevel"/> for the current <paramref name="line"/>.</param>
        /// <param name="elements">Items that replace the tokens found on the <paramref name="line"/>.</param>
        /// <remarks>
        /// This method DO write an End Of Line to the outputs.
        /// <para>The <paramref name="elements"/> array replaces a list of tokens on the <paramref name="line"/> string.</para>
        /// <para>These tokens are in the form of: %@ for index-less parameters and %n for indexed parameters where N is a int value.</para>
        ///	<para>To parse each element, the method will call <see cref="Object.ToString()"/> method for the element, If the call fails, an exception is thrown.</para>
        ///	<para>To write the % symbol into the final entry, use %%.</para>
        /// </remarks>
        ///	<example>
        ///	This example shows how to use the tokens on the <paramref name="line"/> parameter to print the values of the items on the <paramref name="elements"/> array
        ///	<code>
        ///	Write("Item with name %@ costs %@US$. Remember that its name is: %1", LogLevel.Message, "Red Sofa", 79.99f);
        ///	
        /// // This code prints:
        /// //   Item with name Red Sofa costs 79.99US$. Remember that its name is: Red Sofa
        /// </code>
        ///	</example>
        public void WriteColoredLine(string line, ConsoleColor foreground, LogLevel level, params object[] elements)
        {
            unchecked
            {
                Entries++;
                if (level > LogLevel.Error)
                    Errors++;
                else
                {
                    if (level > LogLevel.Warning)
                        Warnings++;
                }
            }

            Proxy.WriteColoredLine(BuildMessageString(line, level), foreground, level, elements);
        }

        /// <summary>
        /// Writes the specified entry into all outputs with a higher or equal <see cref="LogLevel"/>, using the specified Font color.
        /// </summary>
        /// <param name="line">Entry that will be logged.</param>
        /// <param name="foreground">Font color used to display the message</param>
        /// <param name="level"><see cref="LogLevel"/> for the current entry.</param>
        /// <remarks>
        /// This method DO write an End Of Line to the outputs.
        /// </remarks>
        public void WriteColoredLine(string line, ConsoleColor foreground, LogLevel level = LogLevel.Message)
        {
            WriteColoredLine(line, foreground, level, null);
        }

        /// <summary>
        /// Writes the specified entry into all outputs with a higher or equal <see cref="LogLevel"/>, using the specified Background and Font colors.
        /// </summary>
        /// <param name="line">Entry that will be logged.</param>
        /// <param name="background">Background color used to display the message</param>
        /// <param name="foreground">Font color used to display the message</param>
        /// <param name="level"><see cref="LogLevel"/> for the current entry.</param>
        /// <remarks>
        /// This method DO write an End Of Line to the outputs.
        /// </remarks>
        public void WriteColoredLine(string line, ConsoleColor background, ConsoleColor foreground, LogLevel level = LogLevel.Message)
        {
            WriteColoredLine(line, background, foreground, level, null);
        }

        /// <summary>
        /// Writes a new line entry to the outputs with the specified severity, and generates a new Exception of the given type with the same message.
        /// </summary>
        /// <param name="message">Message logged to the outputs and stored into the generated Exception</param>
        /// <param name="severity">Severity for the logged message</param>
        /// <param name="excType">Base Exception type of the new generated Exception instance</param>
        /// <param name="exceptionArguments">Additional arguments passed to the Exception constructor method</param>
        /// <returns>The newly generated exception, ready to be thrown by the calling code</returns>
        /// <remarks>This method is designed to be used directly in a throw statement.</remarks>
        /// <example>throw WriteAndMakeException("Exception message goes here...", LogLevel.Info", typeof(ApplicationException), "Exception argument");</example>
        /// <exception cref="ArgumentException">The method defaults to this Exception when the <paramref name="excType"/> parameter is not a valid Exception type. In this case, the method will also append the condition to the message.</exception>
        public Exception WriteAndMakeException(string message, LogLevel severity, Type excType,
            params object[] exceptionArguments)
        {
            if (excType == null || !excType.IsSubclassOf(typeof(Exception)))
            {
                message = message +
                          "\nAdditionally, com.RazorSoftware.Logging.Log.WriteAndMakeException was called with an invalid excType parameter. The method must be called with a valid Exception (or derived from) class.";
                excType = typeof(ArgumentException);
            }

            ConstructorInfo[] constructors = excType.GetConstructors();
            Exception outputException = null;

            foreach (ConstructorInfo ctor in constructors)
            {
                // Check if the constructor signature matches the specified parameters
                ParameterInfo[] parameters = ctor.GetParameters();

                if (parameters == null || parameters.Length < 1)
                    continue; // Ignore parameterless constructors

                if (!parameters[0].ParameterType.IsAssignableFrom(typeof(string)))
                    continue; // Ignore constructors whose first parameter is not assignable from string

                if (exceptionArguments == null || exceptionArguments.Length < 1)
                {
                    // Call with only a message since no additional arguments where specified
                    if (parameters.Length == 1)
                    {
                        // Valid method signature found. Call it
                        outputException = ctor.Invoke(new object[] { message }) as Exception;
                        break;
                    }
                }
                else
                {
                    // Call with a custom set of parameters
                    if (parameters.Length == 2)
                    {
                        // Check parameters[1] = exceptionArguments
                        if (parameters[1].ParameterType.IsInstanceOfType(exceptionArguments))
                        {
                            // Valid signature found, call it
                            outputException = ctor.Invoke(new object[] { message, exceptionArguments }) as Exception;
                            break;
                        }
                    }

                    if (parameters.Length == exceptionArguments.Length + 1)
                    {
                        // Find a method that matches the whole set of parameters
                        for (int i = 1; i < parameters.Length; i++)
                        {
                            if (!parameters[i].ParameterType.IsInstanceOfType(exceptionArguments[i - 1]))
                                break; // Invalid Signature
                        }

                        // Valid signature found, call it.
                        object[] invocationParameters = new object[exceptionArguments.Length + 1];
                        Array.Copy(exceptionArguments, 0, invocationParameters, 1, invocationParameters.Length - 1);
                        invocationParameters[0] = message;

                        outputException = ctor.Invoke(invocationParameters) as Exception;
                    }
                }
            }

            if (outputException == null)
            {
                message = message +
                          "\nAdditionally, com.RazorSoftware.Logging.Log.WriteAndMakeException was unable to generate an appropriate exception from the given type.";
                outputException = new Exception(message);
            }

            Proxy.WriteLine(message, severity);

            return outputException;
        }

        /// <summary>
        /// Writes information about the provided exception to the outputs
        /// </summary>
        /// <param name="exc">Target exception that will be logged to the outputs</param>
        /// <param name="severity">Severity for the logged message</param>
        /// <remarks>Exception information that will be printed includes: Exception Type (short name) and Exception Message</remarks>
        public void WriteException(Exception exc, LogLevel severity)
        {
            if (exc == null)
                throw new ArgumentNullException("exc");

            StringBuilder str = new StringBuilder();
            str.Append("An exception has been generated. [");
            str.Append(exc.GetType().Name);
            str.Append("] ");
            str.Append(exc.Message);

            Proxy.WriteLine(str.ToString(), severity);
        }

        /// <summary>
        /// Increases the automatic entry <see cref="IndentLevel"/> by the specified amount.
        /// </summary>
        /// <param name="value">Amount to increase.</param>
        public void IncreaseIndent(int value)
        {
            IndentLevel += value;
        }

        /// <summary>
        /// Increases the automatic entry <see cref="IndentLevel"/> by one.
        /// </summary>
        public void IncreaseIndent()
        {
            IndentLevel++;
        }

        /// <summary>
        /// Decreases the automatic entry <see cref="IndentLevel"/> by the specified amount.
        /// </summary>
        /// <param name="value">Amount to decrease.</param>
        public void DecreaseIndent(int value)
        {
            IndentLevel -= value;
        }

        /// <summary>
        /// Decreases the automatic entry <see cref="IndentLevel"/> by one.
        /// </summary>
        public void DecreaseIndent()
        {
            IndentLevel--;

            if (IndentLevel < 0)
                IndentLevel = 0;
        }

        /// <summary>
        /// Resets the automatic entry <see cref="IndentLevel"/> to zero.
        /// </summary>
        public void ResetIndent()
        {
            IndentLevel = 0;
        }

        /// <summary>
        /// Builds a formatted and categorized message from an specified entry and a <see cref="LogLevel"/>.
        /// </summary>
        /// <param name="entry">Base entry for the new Message.</param>
        /// <param name="level"><see cref="LogLevel"/> for the new Message.</param>
        /// <returns>A formatted string that represents the full message with its category and severity tag.</returns>
        private string BuildMessageString(string entry, LogLevel level)
        {
            // Append indentation
            StringBuilder sb = new StringBuilder(new String(' ', IndentLevel * 3));

            // Append Category if defined
            if (!String.IsNullOrEmpty(CategoryName))
            {
                sb.Append('[');
                sb.Append(CategoryName);
                sb.Append(']');
                sb.Append(' ');
            }

            // Append LogLevel if its not LogLevel.Message
            if (level != LogLevel.Message)
            {
                if (level == LogLevel.Crash)
                    sb.Append("[CRASH] ");
                else
                {
                    sb.Append(level.ToString());
                    sb.Append(':');
                    sb.Append(' ');
                }
            }

            // Append the entry
            sb.Append(entry);

            // Return the newly created string
            return sb.ToString();
        }

        /// <summary>
        /// Enables the instance to print a summary of the number of entries, warnings and errors processed, to the log files when they are being closed
        /// </summary>
        public bool CloseStatistics { get; set; } = true;

        // IDisposable Implementation
        /// <summary>
        /// Disposes this instance and frees any used resources.
        /// </summary>
        public void Dispose()
        {
            // Dispose unmanaged resources
            Dispose(true);

            // Suppress Finalization
            GC.SuppressFinalize(this);
        }
        
        /// <summary>
        /// Disposes this instance and any managed/unmanaged resources.
        /// </summary>
        /// <param name="disposing">Indicates if the method should dispose Managed resources.</param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // Managed cleanup
                if (_lastEntryRepeat > 0)
                    WriteLine(); // Force a Flush of the last entry.

                // Print statistics
                if (CloseStatistics)
                    WriteLine(BuildMessageString("Logged %@ entries. %@ total Errors. %@ Warnings.", LogLevel.Message), LogLevel.Message, Entries.ToString("N0"), Errors.ToString("N0"), Warnings.ToString("N0"), LogLevel.Message);

                Proxy = null;
            }
        }
    }
}
