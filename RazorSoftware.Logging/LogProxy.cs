﻿/*
    Copyright © 2018 RazorSoftware
    Fabian R. kderazorback@me.com
    This file is part of RazorSoftware Logging Library.

    RazorSoftware Logging Library is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RazorSoftware Logging Library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RazorSoftware Logging Library.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using com.RazorSoftware.Logging.Outputs;

namespace com.RazorSoftware.Logging
{
    /// <summary>
    /// Provides a Proxy Instance for accessing the Static <see cref="Log"/> class when access to Static members is not possible. This class is a mere wrapper of the <see cref="Log"/> class.
    /// </summary>
    /// <remarks>This instance is used to access the static <see cref="Log"/> class when the Logging Library is loaded in a separate <see cref="AppDomain"/></remarks>
    /// <example>
    /// This example shows how a console application can access the <see cref="Log"/> class when loading the library in a separate <see cref="AppDomain"/>.
    /// <code>
    /// using System;
    /// using System.Reflection;
    /// using com.RazorSoftware.Logging;
    /// 
    /// namespace MyApp
    /// {
    ///     public class Program
    ///     {
    ///         private AppDomain _targetAppDomain;
    /// 
    ///         public void Main()
    ///         {
    ///             TargetAppDomain = AppDomain.CreateDomain("Isolated Logger"); // Create the domain
    ///             TargetAppDomain.Load(Assembly.GetAssembly(typeof (Logger)).GetName()); // Load the assembly
    ///             LogProxy proxy =
    ///                (LogProxy) TargetAppDomain.CreateInstanceAndUnwrap(Assembly.GetAssembly(typeof (LogProxy)).FullName,
    ///                    typeof (LogProxy).FullName); // Create a remote instance of LogProxy (in the separate AppDomain)
    ///             proxy.Initialize(); // Calls Log.Initialize() on the secondary AppDomain
    /// 
    ///             proxy.WriteLine("This message is sent to a remote AppDomain");
    /// 
    ///             proxy.Shutdown(); // Calls Log.Shutdown() on the secondary AppDomain
    ///         }
    ///     }
    /// }
    /// </code>
    /// </example>
#if RAZORSOFTWARE_PUBLIC_API
    public
#else
    internal
#endif
    class LogProxy : MarshalByRefObject
    {
        /// <summary>
        /// Enables or Disables the entire Logging functionality (including all its outputs)
        /// </summary>
        public bool Enabled
        {
            get { return Log.Enabled; }
            set { Log.Enabled = value; }
        }
        /// <summary>
        /// Store files that will be used as outputs for logged messages.
        /// </summary>
        /// <remarks>
        /// If no files are specified, File logging will be disabled.
        /// </remarks>
        public ReadOnlyCollection<FileOutput> Files
        {
            get { return Log.Files; }
        }
        /// <summary>
        /// Stores a console instance that will be used as output for logged messages.
        /// </summary>
        /// <remarks>
        /// Only one Console Output can be active at a time.
        /// </remarks>
        public ConsoleOutput Console 
        {
            get { return Log.Console; }
        }
        /// <summary>
        /// Stores a debugger instance that will be used as output for logged messages.
        /// </summary>
        /// <remarks>
        /// Only one Debugger Output can be active at a time.
        /// </remarks>
        public DebuggerOutput Debugger
        {
            get { return Log.Debugger; }
        }
        /// <summary>
        /// Stores the amount of error messages that where sent to the Log since its initialization.
        /// </summary>
        /// <remarks>This attribute is Arithmetically unchecked for overflows.</remarks>
        public int Errors
        {
            get { return Log.Errors; }
        }
        /// <summary>
        /// Stores the amount of warning messages that where sent to the Log since its initialization.
        /// </summary>
        /// <remarks>This attribute is Arithmetically unchecked for overflows.</remarks>
        public int Warnings
        {
            get { return Log.Warnings; }
        }
        /// <summary>
        /// Stores the amount of entries that where sent to the Log since its initialization.
        /// </summary>
        /// <remarks>This attribute is Arithmetically unchecked for overflows.</remarks>
        public int Entries
        {
            get { return Log.Entries; }
        }
        /// <summary>
        /// Disables entirely Thread-Safety in this class.
        /// </summary>
        /// <exception cref="InvalidOperationException">Thrown when this attribute is being modified when the Instance is already Initialized.</exception>
        /// <remarks>
        /// This could lead to malformed entries being written to the outputs.
        /// <para>This attribute cannot be modified once the instance is Initialized.</para>
        /// </remarks>
        public bool SuppressSecurity
        {
            get { return Log.SuppressSecurity; }
            set { Log.SuppressSecurity = value; }
        }
        /// <summary>
        /// When this attribute is set, a more lightweight and fast thread-locking primitive will be used for managing multiple access to the outputs, which will improve the Log overall performance, but will cause issues and unexpected exceptions when using multiple <see cref="AppDomain"/>.
        /// </summary>
        /// <exception cref="InvalidOperationException">Thrown when this attribute is being modified when the Instance is already Initialized.</exception>
        /// <remarks>
        /// This attribute cannot be modified once the instance is Initialized.
        /// </remarks>
        public bool UseLocalSecurity
        {
            get { return Log.UseLocalSecurity; }
            set { Log.UseLocalSecurity = value; }
        }

        /// <summary>
        /// Stores the Application Name that will be displayed on Outputs headers.
        /// </summary>
        /// <remarks>
        /// If this value is Null or an Empty String when the instance Initializes, a default name will be extracted from the current Assembly Manifest.
        /// </remarks>
        public string AppName
        {
            get { return Log.AppName; }
            set { Log.AppName = value; }
        }

        /// <summary>
        /// Stores the Application Version that will be logged to the Outputs.
        /// </summary>
        /// <remarks>
        /// If this value is Null or an Empty String when the instance Initializes, a default version will be extracted from the current Assembly Manifest
        /// </remarks>
        public string AppVersion
        {
            get { return Log.AppVersion; }
            set { Log.AppVersion = value; }
        }
        /// <summary>
        /// Stores a list of Custom objects that will be used as Outputs for logged Messages
        /// </summary>
        public ReadOnlyCollection<IOutput> CustomOutputs
        {
            get
            {
                return Log.CustomOutputs;
            }
        }
        /// <summary>
        /// Enables the AutoWrapping functionality for all lines written to Outputs. When this feature is enabled, repetitions for a single message will be ignored and will not appear on any output until a new message is written.
        /// When a new message is sent, this class will automatically print a summary of how many times the first message appeared before writting the second one.
        /// This summary is printed by using the template from <see cref="AutoWrappingReportString"/> and by replacing its single parameter with the repetition count. The loglevel of this message can be specified on the <see cref="AutoWrappingReportLogLevel"/> property.
        /// When this feature is enabled, you must call <see cref="Shutdown"/> before closing any outputs to avoid missing any remaining summary message.
        /// </summary>
        public bool AutoWrappingEnabled
        {
            get
            {
                return Log.AutoWrappingEnabled;
            }
            set
            {
                Log.AutoWrappingEnabled = value;
            }
        }
        /// <summary>
        /// Specifies the <see cref="LogLevel"/> of the report message written for string repetitions, when the <see cref="AutoWrappingEnabled"/> feature is enabled.
        /// </summary>
        public LogLevel AutoWrappingReportLogLevel
        {
            get
            {
                return Log.AutoWrappingReportLogLevel;
            }
            set
            {
                Log.AutoWrappingReportLogLevel = value;
            }
        }
        /// <summary>
        /// Specifies the string template to be used when printing summaries for string repetitions, when the <see cref="AutoWrappingEnabled"/> feature is enabled.
        /// This string is expanded internally and can contain a single token or placeholder in the form of "%@" or "%1" for printing the Repetition count.
        /// </summary>
        public string AutoWrappingReportString
        {
            get
            {
                return Log.AutoWrappingReportString;
            }
            set
            {
                Log.AutoWrappingReportString = value;
            }
        }
        /// <summary>
        /// Enables the instance to print a summary of the number of entries, warnings and errors processed, to the log files when they are being closed
        /// </summary>
        public static bool CloseStatistics
        {
            get
            {
                return Log.CloseStatistics;
            }
            set
            {
                Log.CloseStatistics = value;
            }
        }

        // Methods
        /// <summary>
        /// Opens the specified file as an output for logged messages, applying the specified <see cref="LogLevel"/> as filter.
        /// </summary>
        /// <exception cref="System.IO.FileNotFoundException">Thrown when the path to the target file cannot be found, or if <paramref name="append"/> is set and the file doesnt exists.</exception>
        /// <exception cref="System.IO.IOException">Thrown when the target file cannot be created or opened for Writting.</exception>
        /// <param name="fileName">Path to the file that will be opened.</param>
        /// <param name="identifier">This identifier will be used to name the File as an Output.</param>
        /// <param name="level">Minimum Logging Level that the entries must have to be logged on this newly created Output.</param>
        /// <param name="append">If append parameter is set, then the logger will seek to the end of the file after opening it and append all new logged messages to the end. If the parameter is not set, the file will be erased when opened.</param>
        public void OpenFile(string fileName, string identifier, LogLevel level, bool append)
        {
            Log.OpenFile(fileName, identifier, level, append);
        }

        /// <summary>
        /// Appends footer data to the specified previously opened file and then closes it.
        /// </summary>
        /// <exception cref="KeyNotFoundException">Thrown when the file with the specified identifier cannot be found.</exception>
        /// <exception cref="IOException">Thrown when file cannot be closed.</exception>
        /// <param name="identifier">Identifier of the file to be closed.</param>
        public void CloseFile(string identifier)
        {
            Log.CloseFile(identifier);
        }

        /// <summary>
        /// Appends footer data to the specified previously opened file (by index) and then closes it.
        /// </summary>
        /// <exception cref="IndexOutOfRangeException">Thrown when the index supplied is outside the limits of the outputs array.</exception>
        /// <exception cref="IOException">Thrown when file cannot be closed.</exception>
        /// <param name="index">Index of the File output to be closed.</param>
        public void CloseFile(int index)
        {
            Log.CloseFile(index);
        }

        /// <summary>
        /// Registers a Custom object for receiving messages logged via this class
        /// </summary>
        /// <param name="output">Custom output that will be Attached</param>
        /// <exception cref="ArgumentNullException">Thrown when the output argument is Null</exception>
        /// <exception cref="ArgumentException">Thrown when the specified object is already attached to this Class, or there is another object with the same Identifier already registered</exception>
        public void AttachOutput(IOutput output)
        {
            Log.AttachOutput(output);
        }

        /// <summary>
        /// Frees a reference to a previously attached Object, and stops logging messages to it
        /// </summary>
        /// <param name="output">Target output that will be Detached</param>
        /// <exception cref="ArgumentNullException">Thrown when the output argument is Null</exception>
        /// <exception cref="KeyNotFoundException">Thrown when the specified object is not registered on this Class</exception>
        public void DetachOutput(IOutput output)
        {
            Log.DetachOutput(output);
        }

        /// <summary>
        /// Frees a reference to a previously attached Object that matches the specified Identifier, and stops logging messages to it
        /// </summary>
        /// <param name="identifier">Target output that will be Detached</param>
        /// <exception cref="ArgumentNullException">Thrown when the Identifier argument is Null</exception>
        /// <exception cref="KeyNotFoundException">Thrown when the specified object is not registered on this Class</exception>
        public void DetachOutput(string identifier)
        {
            Log.DetachOutput(identifier);
        }

        /// <summary>
        /// Frees all custom objects attached to this Class that are being used as logging Outputs
        /// </summary>
        public void DetachOutputs()
        {
            Log.DetachOutputs();
        }

        /// <summary>
        /// Writes the specified entry into outputs with a higher or equal <see cref="LogLevel"/>.
        /// </summary>
        /// <exception cref="ArgumentException">Thrown when <see cref="Object.ToString()"/> cannot be called for an item inside the elements parameter.</exception>
        /// <exception cref="IOException">Thrown when the entry cannot be written to an Output.</exception>
        /// <param name="entry">Entry that will be logged.</param>
        /// <param name="level"><see cref="LogLevel"/> for the current <paramref name="entry"/>.</param>
        /// <param name="elements">Items that replace the tokens found on the <paramref name="entry"/>.</param>
        /// <remarks>
        /// This method DONT write an End Of Line to the outputs.
        /// <para>The <paramref name="elements"/> array replaces a list of tokens on the <paramref name="entry"/> string.</para>
        /// <para>These tokens are in the form of: %@ for index-less parameters and %n for indexed parameters where N is a int value ranging from 0 to 9 inclusive.</para>
        ///	<para>To parse each element, the method will call <see cref="Object.ToString()"/> method for the element, If the call fails, an exception is thrown.</para>
        ///	<para>To write the % symbol into the final entry, use %%.</para>
        /// </remarks>
        ///	<example>
        ///	This example shows how to use the tokens on the <paramref name="entry"/> parameter to print the values of the items on the <paramref name="elements"/> array
        ///	<code>
        ///	Write("Item with name %@ costs %@US$. Remember that its name is: %1", LogLevel.Message, "Red Sofa", 79.99f);
        ///	
        /// // This code prints:
        /// //   Item with name Red Sofa costs 79.99US$. Remember that its name is: Red Sofa
        /// </code>
        ///	</example>
        public void Write(string entry, LogLevel level, params object[] elements)
        {
            Log.Write(entry, level, elements);
        }

        /// <summary>
        /// Writes the specified entry into outputs with a higher or equal <see cref="LogLevel"/>.
        /// </summary>
        /// <exception cref="IOException">Thrown when the entry cannot be written to an Output.</exception>
        /// <param name="entry">Entry that will be logged.</param>
        /// <param name="level"><see cref="LogLevel"/> for the current entry.</param>
        /// <remarks>
        /// This method DONT write an End Of Line to the outputs.
        /// </remarks>
        public void Write(string entry, LogLevel level)
        {
            Log.Write(entry, level);
        }

        /// <summary>
        /// Writes the specified entry into outputs with a <see cref="LogLevel"/> of <see cref="LogLevel.Message"/> or higher.
        /// </summary>
        /// <exception cref="IOException">Thrown when the entry cannot be written to an Output.</exception>
        /// <param name="entry">Entry that will be logged.</param>
        /// <remarks>
        /// This method DONT write an End Of Line to the outputs.
        /// </remarks>
        public void Write(string entry)
        {
            Log.Write(entry);
        }

        /// <summary>
        /// Writes the specified entry into all outputs with a higher or equal <see cref="LogLevel"/> using the specified Background and Font colors. This variant replaces tokens in the form of %@ or %i (one-based) with the arguments provided in <paramref name="elements"/>>.
        /// </summary>
        /// <exception cref="ArgumentException">Thrown when <see cref="Object.ToString()"/> cannot be called for an item inside the elements parameter.</exception>
        /// <exception cref="IOException">Thrown when the entry cannot be written to an Output.</exception>
        /// <param name="entry">Entry that will be logged.</param>
        /// <param name="background">Background color used to display the message</param>
        /// <param name="foreground">Font color used to display the message</param>
        /// <param name="level"><see cref="LogLevel"/> for the current <paramref name="entry"/>.</param>
        /// <param name="elements">Items that replace the tokens found on the <paramref name="entry"/>.</param>
        /// <remarks>
        /// This method DONT write an End Of Line to the outputs.
        /// <para>The <paramref name="elements"/> array replaces a list of tokens on the <paramref name="entry"/> string.</para>
        /// <para>These tokens are in the form of: %@ for index-less parameters and %n for indexed parameters where N is a int value.</para>
        ///	<para>To parse each element, the method will call <see cref="Object.ToString()"/> method for the element, If the call fails, an exception is thrown.</para>
        ///	<para>To write the % symbol into the final entry, use %%.</para>
        /// </remarks>
        ///	<example>
        ///	This example shows how to use the tokens on the <paramref name="entry"/> parameter to print the values of the items on the <paramref name="elements"/> array
        ///	<code>
        ///	Write("Item with name %@ costs %@US$. Remember that its name is: %1", LogLevel.Message, "Red Sofa", 79.99f);
        ///	
        /// // This code prints:
        /// //   Item with name Red Sofa costs 79.99US$. Remember that its name is: Red Sofa
        /// </code>
        ///	</example>
        public void WriteColored(string entry, ConsoleColor background, ConsoleColor foreground, LogLevel level, params object[] elements)
        {
            Log.WriteColored(entry, background, foreground, level, elements);
        }

        /// <summary>
        /// Writes the specified entry into all outputs with a higher or equal <see cref="LogLevel"/> using the specified Font color. This variant replaces tokens in the form of %@ or %i (one-based) with the arguments provided in <paramref name="elements"/>>.
        /// </summary>
        /// <exception cref="ArgumentException">Thrown when <see cref="Object.ToString()"/> cannot be called for an item inside the elements parameter.</exception>
        /// <exception cref="IOException">Thrown when the entry cannot be written to an Output.</exception>
        /// <param name="entry">Entry that will be logged.</param>
        /// <param name="foreground">Font color used to display the message</param>
        /// <param name="level"><see cref="LogLevel"/> for the current <paramref name="entry"/>.</param>
        /// <param name="elements">Items that replace the tokens found on the <paramref name="entry"/>.</param>
        /// <remarks>
        /// This method DONT write an End Of Line to the outputs.
        /// <para>The <paramref name="elements"/> array replaces a list of tokens on the <paramref name="entry"/> string.</para>
        /// <para>These tokens are in the form of: %@ for index-less parameters and %n for indexed parameters where N is a int value.</para>
        ///	<para>To parse each element, the method will call <see cref="Object.ToString()"/> method for the element, If the call fails, an exception is thrown.</para>
        ///	<para>To write the % symbol into the final entry, use %%.</para>
        /// </remarks>
        ///	<example>
        ///	This example shows how to use the tokens on the <paramref name="entry"/> parameter to print the values of the items on the <paramref name="elements"/> array
        ///	<code>
        ///	Write("Item with name %@ costs %@US$. Remember that its name is: %1", LogLevel.Message, "Red Sofa", 79.99f);
        ///	
        /// // This code prints:
        /// //   Item with name Red Sofa costs 79.99US$. Remember that its name is: Red Sofa
        /// </code>
        ///	</example>
        public void WriteColored(string entry, ConsoleColor foreground, LogLevel level, params object[] elements)
        {
            Log.WriteColored(entry, foreground, level, elements);
        }

        /// <summary>
        /// Writes the specified entry into all outputs with a higher or equal <see cref="LogLevel"/> with the specified Background and Font colors.
        /// </summary>
        /// <exception cref="IOException">Thrown when the entry cannot be written to an Output.</exception>
        /// <param name="entry">Entry that will be logged.</param>
        /// <param name="background">Background color used to display the message</param>
        /// <param name="foreground">Font color used to display the message</param>
        /// <param name="level"><see cref="LogLevel"/> for the current entry.</param>
        /// <remarks>
        /// This method DONT write an End Of Line to the outputs.
        /// </remarks>
        public void WriteColored(string entry, ConsoleColor background, ConsoleColor foreground, LogLevel level = LogLevel.Message)
        {
            Log.WriteColored(entry, background, foreground, level, null);
        }

        /// <summary>
        /// Writes the specified entry into all outputs with a higher or equal <see cref="LogLevel"/> with the specified Font color.
        /// </summary>
        /// <exception cref="IOException">Thrown when the entry cannot be written to an Output.</exception>
        /// <param name="entry">Entry that will be logged.</param>
        /// <param name="foreground">Font color used to display the message</param>
        /// <param name="level"><see cref="LogLevel"/> for the current entry.</param>
        /// <remarks>
        /// This method DONT write an End Of Line to the outputs.
        /// </remarks>
        public void WriteColored(string entry, ConsoleColor foreground, LogLevel level = LogLevel.Message)
        {
            Log.WriteColored(entry, foreground, level, null);
        }

        /// <summary>
        /// Writes the specified entry into outputs with a higher or equal <see cref="LogLevel"/>.
        /// </summary>
        /// <exception cref="ArgumentException">Thrown when <see cref="Object.ToString()"/> cannot be called for an item inside the elements parameter.</exception>
        /// <exception cref="IOException">Thrown when the entry cannot be written to an Output.</exception>
        /// <param name="line">Entry that will be logged.</param>
        /// <param name="level"><see cref="LogLevel"/> for the current <paramref name="line"/>.</param>
        /// <param name="elements">Items that replace the tokens found on the <paramref name="line"/>.</param>
        /// <remarks>
        /// This method DO write an End Of Line to the outputs.
        /// <para>The <paramref name="elements"/> array replaces a list of tokens on the <paramref name="line"/> string.</para>
        /// <para>These tokens are in the form of: %@ for index-less parameters and %n for indexed parameters where N is a int value ranging from 0 to 9 inclusive.</para>
        ///	<para>To parse each element, the method will call <see cref="Object.ToString()"/> method for the element, If the call fails, an exception is thrown.</para>
        ///	<para>To write the % symbol into the final entry, use %%.</para>
        /// </remarks>
        ///	<example>
        ///	This example shows how to use the tokens on the <paramref name="line"/> parameter to print the values of the items on the <paramref name="elements"/> array
        ///	<code>
        ///	Write("Item with name %@ costs %@US$. Remember that its name is: %1", LogLevel.Message, "Red Sofa", 79.99f);
        ///	
        /// // This code prints:
        /// //   Item with name Red Sofa costs 79.99US$. Remember that its name is: Red Sofa
        /// </code>
        ///	</example>
        public void WriteLine(string line, LogLevel level, params object[] elements)
        {
            Log.WriteLine(line, level, elements);
        }

        /// <summary>
        /// Writes the specified entry into outputs with a higher or equal <see cref="LogLevel"/>.
        /// </summary>
        /// <param name="line">Entry that will be logged.</param>
        /// <param name="level"><see cref="LogLevel"/> for the current entry.</param>
        /// <remarks>
        /// This method DO write an End Of Line to the outputs.
        /// </remarks>
        public void WriteLine(string line, LogLevel level)
        {
            Log.WriteLine(line, level);
        }

        /// <summary>
        /// Writes the specified entry into outputs with a <see cref="LogLevel"/> of <see cref="LogLevel.Message"/> or higher.
        /// </summary>
        /// <param name="line">Entry that will be logged.</param>
        /// <remarks>
        /// This method DO write an End Of Line to the outputs.
        /// </remarks>
        public void WriteLine(string line)
        {
            Log.WriteLine(line);
        }

        /// <summary>
        /// Writes a new blank line entry into outputs with a higher or equal <see cref="LogLevel"/>.
        /// </summary>
        /// <param name="level"><see cref="LogLevel"/> for the new blank line.</param>
        public void WriteLine(LogLevel level)
        {
            Log.WriteLine(level);
        }

        /// <summary>
        /// Writes a new blank line entry into the outputs with a <see cref="LogLevel"/> of <see cref="LogLevel.Message"/>.
        /// </summary>
        public void WriteLine()
        {
            Log.WriteLine();
        }

        /// <summary>
        /// Writes the specified entry into outputs with a higher or equal <see cref="LogLevel"/> using the specified Background and Font colors. This variant replaces tokens in the form of %@ or %i (one-based) with the arguments provided in <paramref name="elements"/>>.
        /// </summary>
        /// <exception cref="ArgumentException">Thrown when <see cref="Object.ToString()"/> cannot be called for an item inside the elements parameter.</exception>
        /// <exception cref="IOException">Thrown when the entry cannot be written to an Output.</exception>
        /// <param name="line">Entry that will be logged.</param>
        /// <param name="background">Background color used to display the message</param>
        /// <param name="foreground">Font color used to display the message</param>
        /// <param name="level"><see cref="LogLevel"/> for the current <paramref name="line"/>.</param>
        /// <param name="elements">Items that replace the tokens found on the <paramref name="line"/>.</param>
        /// <remarks>
        /// This method DO write an End Of Line to the outputs.
        /// <para>The <paramref name="elements"/> array replaces a list of tokens on the <paramref name="line"/> string.</para>
        /// <para>These tokens are in the form of: %@ for index-less parameters and %n for indexed parameters where N is a int value.</para>
        ///	<para>To parse each element, the method will call <see cref="Object.ToString()"/> method for the element, If the call fails, an exception is thrown.</para>
        ///	<para>To write the % symbol into the final entry, use %%.</para>
        /// </remarks>
        ///	<example>
        ///	This example shows how to use the tokens on the <paramref name="line"/> parameter to print the values of the items on the <paramref name="elements"/> array
        ///	<code>
        ///	Write("Item with name %@ costs %@US$. Remember that its name is: %1", LogLevel.Message, "Red Sofa", 79.99f);
        ///	
        /// // This code prints:
        /// //   Item with name Red Sofa costs 79.99US$. Remember that its name is: Red Sofa
        /// </code>
        ///	</example>
        public void WriteColoredLine(string line, ConsoleColor background, ConsoleColor foreground, LogLevel level, params object[] elements)
        {
            Log.WriteColoredLine(line, background, foreground, level, elements);
        }

        /// <summary>
        /// Writes the specified entry into all outputs with a higher or equal <see cref="LogLevel"/> using the specified Font color. This variant replaces tokens in the form of %@ or %i (one-based) with the arguments provided in <paramref name="elements"/>>.
        /// </summary>
        /// <exception cref="ArgumentException">Thrown when <see cref="Object.ToString()"/> cannot be called for an item inside the elements parameter.</exception>
        /// <exception cref="IOException">Thrown when the entry cannot be written to an Output.</exception>
        /// <param name="line">Entry that will be logged.</param>
        /// <param name="foreground">Font color used to display the message</param>
        /// <param name="level"><see cref="LogLevel"/> for the current <paramref name="line"/>.</param>
        /// <param name="elements">Items that replace the tokens found on the <paramref name="line"/>.</param>
        /// <remarks>
        /// This method DO write an End Of Line to the outputs.
        /// <para>The <paramref name="elements"/> array replaces a list of tokens on the <paramref name="line"/> string.</para>
        /// <para>These tokens are in the form of: %@ for index-less parameters and %n for indexed parameters where N is a int value.</para>
        ///	<para>To parse each element, the method will call <see cref="Object.ToString()"/> method for the element, If the call fails, an exception is thrown.</para>
        ///	<para>To write the % symbol into the final entry, use %%.</para>
        /// </remarks>
        ///	<example>
        ///	This example shows how to use the tokens on the <paramref name="line"/> parameter to print the values of the items on the <paramref name="elements"/> array
        ///	<code>
        ///	Write("Item with name %@ costs %@US$. Remember that its name is: %1", LogLevel.Message, "Red Sofa", 79.99f);
        ///	
        /// // This code prints:
        /// //   Item with name Red Sofa costs 79.99US$. Remember that its name is: Red Sofa
        /// </code>
        ///	</example>
        public void WriteColoredLine(string line, ConsoleColor foreground, LogLevel level, params object[] elements)
        {
            Log.WriteColoredLine(line, foreground, level, elements);
        }

        /// <summary>
        /// Writes the specified entry into all outputs with a higher or equal <see cref="LogLevel"/>, using the specified Font color.
        /// </summary>
        /// <param name="line">Entry that will be logged.</param>
        /// <param name="foreground">Font color used to display the message</param>
        /// <param name="level"><see cref="LogLevel"/> for the current entry.</param>
        /// <remarks>
        /// This method DO write an End Of Line to the outputs.
        /// </remarks>
        public void WriteColoredLine(string line, ConsoleColor foreground, LogLevel level = LogLevel.Message)
        {
            Log.WriteColoredLine(line, foreground, level, null);
        }

        /// <summary>
        /// Writes the specified entry into all outputs with a higher or equal <see cref="LogLevel"/>, using the specified Background and Font colors.
        /// </summary>
        /// <param name="line">Entry that will be logged.</param>
        /// <param name="background">Background color used to display the message</param>
        /// <param name="foreground">Font color used to display the message</param>
        /// <param name="level"><see cref="LogLevel"/> for the current entry.</param>
        /// <remarks>
        /// This method DO write an End Of Line to the outputs.
        /// </remarks>
        public void WriteColoredLine(string line, ConsoleColor background, ConsoleColor foreground, LogLevel level = LogLevel.Message)
        {
            Log.WriteColoredLine(line, background, foreground, level, null);
        }

        /// <summary>
        /// Writes a new line entry to the outputs with the specified severity, and generates a new Exception of the given type with the same message.
        /// </summary>
        /// <param name="message">Message logged to the outputs and stored into the generated Exception</param>
        /// <param name="severity">Severity for the logged message</param>
        /// <param name="excType">Base Exception type of the new generated Exception instance</param>
        /// <param name="exceptionArguments">Additional arguments passed to the Exception constructor method</param>
        /// <returns>The newly generated exception, ready to be thrown by the calling code</returns>
        /// <remarks>This method is designed to be used directly in a throw statement.</remarks>
        /// <example>throw WriteAndMakeException("Exception message goes here...", LogLevel.Info", typeof(ApplicationException), "Exception argument");</example>
        /// <exception cref="ArgumentException">The method defaults to this Exception when the <paramref name="excType"/> parameter is not a valid Exception type. In this case, the method will also append the condition to the message.</exception>
        public Exception WriteAndMakeException(string message, LogLevel severity, Type excType,
            params object[] exceptionArguments)
        {
            return Log.WriteAndMakeException(message, severity, excType, exceptionArguments);
        }

        /// <summary>
        /// Writes information about the provided exception to the outputs
        /// </summary>
        /// <param name="exc">Target exception that will be logged to the outputs</param>
        /// <param name="severity">Severity for the logged message</param>
        /// <remarks>Exception information that will be printed includes: Exception Type (short name) and Exception Message</remarks>
        public void WriteException(Exception exc, LogLevel severity)
        {
            Log.WriteException(exc, severity);
        }

        /// <summary>
        /// Initializes this class and prepares some default outputs for entry logging.
        /// </summary>
        /// <exception cref="System.IO.IOException">Thrown when an IOException occurs on <see cref="OpenFile"/> while opening default outputs.</exception>
        /// <param name="openDefaultOutputs">Indicates if default outputs will be opened.</param>
        /// <remarks>
        /// If <paramref name="openDefaultOutputs"/> is not set, no output will be opened, but the Logger will still be initialized.
        /// <para></para>
        /// <para>These Standard Outputs differ from BuildType and are:</para>
        ///	<para>For Debug builds:</para>
        ///	<para>	A <see cref="ConsoleOutput"/> with <see cref="LogLevel.Debug"/></para>
        ///	<para>	A <see cref="DebuggerOutput"/> with <see cref="LogLevel.Warning"/></para>
        ///	<para>	A <see cref="FileOutput"/> with <see cref="LogLevel.Debug"/></para>
        ///	<para>	A <see cref="FileOutput"/> with <see cref="LogLevel.Message"/></para>
        ///	<para>For Release builds:</para>
        ///	<para>	A <see cref="FileOutput"/> with <see cref="LogLevel.Message"/></para>
        ///	<para>	A <see cref="DebuggerOutput"/> with <see cref="LogLevel.Error"/></para>
        /// </remarks>
        public void Initialize(bool openDefaultOutputs)
        {
            Log.Initialize(openDefaultOutputs);
        }

        /// <summary>
        /// Initializes this class and prepares some default outputs for entry logging.
        /// </summary>
        /// <remarks>
        /// These Standard Outputs differ from BuildType and are:
        ///	<para>For Debug builds:</para>
        ///	<para>	A <see cref="ConsoleOutput"/> with <see cref="LogLevel.Debug"/></para>
        ///	<para>	A <see cref="DebuggerOutput"/> with <see cref="LogLevel.Warning"/></para>
        ///	<para>	A <see cref="FileOutput"/> with <see cref="LogLevel.Debug"/></para>
        ///	<para>	A <see cref="FileOutput"/> with <see cref="LogLevel.Message"/></para>
        ///	<para>For Release builds:</para>
        ///	<para>	A <see cref="FileOutput"/> with <see cref="LogLevel.Message"/></para>
        ///	<para>	A <see cref="DebuggerOutput"/> with <see cref="LogLevel.Error"/></para>
        /// </remarks>
        public void Initialize()
        {
            Log.Initialize();
        }

        /// <summary>
        /// Closes all outputs associated to this Log and frees any used resources
        /// </summary>
        /// <exception cref="ObjectDisposedException">Thrown when the instance has already shut down.</exception>
        public void Shutdown()
        {
            Log.Shutdown();
        }

        /// <summary>
        /// Replaces all tokens in the specified string with the Description of the objects provided.
        /// </summary>
        /// <param name="entry">Source string with tokens for replace</param>
        /// <param name="elements">Array of objects that replaces the tokes on the entry parameter</param>
        /// <returns>An string with token information replaced with the objects on the elements array</returns>
        /// <remarks>If the <paramref name="elements"/> array is empty (no additional parameters were passed), the method will return the original entry unmodified.
        /// The first 15 Tokens can be referenced by its index in hexadecimal form (from 1 through F), but more tokens can be referenced by the positional auto-increment index (@).</remarks>
        public string ExpandStrTokens(string entry, params object[] elements)
        {
            return Log.ExpandStrTokens(entry, elements);
        }
    }
}
