﻿/*
    Copyright © 2018 RazorSoftware
    Fabian R. kderazorback@me.com
    This file is part of RazorSoftware Logging Library.

    RazorSoftware Logging Library is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RazorSoftware Logging Library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RazorSoftware Logging Library.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.IO;

namespace com.RazorSoftware.Logging.Outputs
{
    /// <summary>
    /// Provides common functionality and generalization for Log Outputs. This class is abstract.
    /// </summary>
#if RAZORSOFTWARE_PUBLIC_API
    public abstract class LogOutput : MarshalByRefObject
#else
    internal abstract class LogOutput : MarshalByRefObject
#endif
    {
        // Attributes
        /// <summary>
        /// Stores the minimum <see cref="LogLevel"/> that will be logged by this instance.
        /// </summary>
        public LogLevel MinimumLevel { get; set; }
        /// <summary>
        /// Specifies if this instance if enabled and will process Log entries.
        /// </summary>
        public bool Enabled { get; set; }

        // Methods
        /// <summary>
        /// Writes the specified entry to the Output.
        /// </summary>
        /// <exception cref="IOException">Thrown when An I/O error occurred.</exception>
        /// <param name="entry">Text that will be written to the Output</param>
        /// <remarks>
        /// This method DONT write an end of line to the output.
        /// </remarks>
        public abstract void Write(string entry);

        /// <summary>
        /// Writes the specified entry to the Output with the indicated severity.
        /// </summary>
        /// <exception cref="IOException">Thrown when An I/O error occurred.</exception>
        /// <param name="entry">Text that will be written to the Output</param>
        /// <param name="severity">Severity for the logged message</param>
        /// <remarks>
        /// This method DONT write an end of line to the output.
        /// </remarks>
        public virtual void Write(string entry, LogLevel severity)
        {
            Write(entry);
        }

        /// <summary>
        /// Writes the specified entry into the Output, replacing any token found with an item on the elements array.
        /// </summary>
        /// <exception cref="ArgumentException">Thrown when <see cref="Object.ToString()"/> cannot be called for an item inside the elements parameter.</exception>
        /// <exception cref="IOException">Thrown when the entry cannot be written to the Output.</exception>
        /// <param name="entry">Entry that will be written to the Output.</param>
        /// <param name="elements">Items that replace the tokens found on the <paramref name="entry"/>.</param>
        /// <remarks>
        /// This method DOESNT write an End Of Line to the output.
        /// <para>The <paramref name="elements"/> array replaces a list of tokens on the <paramref name="entry"/> string.</para>
        /// <para>These tokens are in the form of: %@ for index-less parameters and %n for indexed parameters where N is a int value ranging from 0 to 9 inclusive.</para>
        ///	<para>To parse each element, the method will call <see cref="Object.ToString()"/> method for the element, If the call fails, an exception is thrown.</para>
        ///	<para>To write the % symbol into the final entry, use %%.</para>
        /// </remarks>
        ///	<example>
        ///	This example shows how to use the tokens on the <paramref name="entry"/> parameter to print the values of the items on the <paramref name="elements"/> array
        ///	<code>
        ///	Write("Item with name %@ costs %@US$. Remember that its name is: %1", LogLevel.Message, "Red Sofa", 79.99f);
        ///	
        /// // This code prints:
        /// //   Item with name Red Sofa costs 79.99US$. Remember that its name is: Red Sofa
        /// </code>
        ///	</example>
        public virtual void Write(string entry, params object[] elements)
        {
            if (!Enabled)
                return;

            if (elements == null || elements.Length < 1)
                Write(entry);
            else
                Write(Log.ExpandStrTokens(entry, elements));
        }

        /// <summary>
        /// Writes the specified entry to the Output.
        /// </summary>
        /// <exception cref="IOException">Thrown when An I/O error occurred.</exception>
        /// <param name="entry">Text that will be written to the Output</param>
        /// <remarks>
        /// This method DO write an end of line to the output.
        /// </remarks>
        public abstract void WriteLine(string entry);

        /// <summary>
        /// Writes the specified entry to the Output with the indicated severity.
        /// </summary>
        /// <exception cref="IOException">Thrown when An I/O error occurred.</exception>
        /// <param name="entry">Text that will be written to the Output</param>
        /// <param name="severity">Severity for the logged message</param>
        /// <remarks>
        /// This method DO write an end of line to the output.
        /// </remarks>
        public virtual void WriteLine(string entry, LogLevel severity)
        {
            WriteLine(entry);
        }

        /// <summary>
        /// Writes the specified entry into the Output, replacing any token found with an item on the elements array.
        /// </summary>
        /// <exception cref="ArgumentException">Thrown when <see cref="Object.ToString()"/> cannot be called for an item inside the elements parameter.</exception>
        /// <exception cref="IOException">Thrown when the entry cannot be written to the Output.</exception>
        /// <param name="entry">Entry that will be written to the Output.</param>
        /// <param name="elements">Items that replace the tokens found on the <paramref name="entry"/>.</param>
        /// <remarks>
        /// This method DO write an End Of Line to the output.
        /// <para>The <paramref name="elements"/> array replaces a list of tokens on the <paramref name="entry"/> string.</para>
        /// <para>These tokens are in the form of: %@ for index-less parameters and %n for indexed parameters where N is a int value ranging from 0 to 9 inclusive.</para>
        ///	<para>To parse each element, the method will call <see cref="Object.ToString()"/> method for the element, If the call fails, an exception is thrown.</para>
        ///	<para>To write the % symbol into the final entry, use %%.</para>
        /// </remarks>
        ///	<example>
        ///	This example shows how to use the tokens on the <paramref name="entry"/> parameter to print the values of the items on the <paramref name="elements"/> array
        ///	<code>
        ///	Write("Item with name %@ costs %@US$. Remember that its name is: %1", LogLevel.Message, "Red Sofa", 79.99f);
        ///	
        /// // This code prints:
        /// //   Item with name Red Sofa costs 79.99US$. Remember that its name is: Red Sofa
        /// </code>
        ///	</example>
        public virtual void WriteLine(string entry, params object[] elements)
        {
            if (!Enabled)
                return;

            if (elements == null || elements.Length < 1)
                WriteLine(entry);
            else
                WriteLine(Log.ExpandStrTokens(entry, elements));
        }

    }
}
