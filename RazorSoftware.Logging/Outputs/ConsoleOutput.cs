﻿/*
    Copyright © 2018 RazorSoftware
    Fabian R. kderazorback@me.com
    This file is part of RazorSoftware Logging Library.

    RazorSoftware Logging Library is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RazorSoftware Logging Library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RazorSoftware Logging Library.  If not, see <http://www.gnu.org/licenses/>.
 */


using System;
using System.IO;
using System.Text;
using Microsoft.Win32.SafeHandles;

namespace com.RazorSoftware.Logging.Outputs
{
    /// <summary>
    /// Allows the <see cref="Console"/> to be used as Output for entries sent to the <see cref="Log"/> Class.
    /// </summary>
#if RAZORSOFTWARE_PUBLIC_API
    public class ConsoleOutput : LogOutput, IDisposable
#else
    internal class ConsoleOutput : LogOutput, IDisposable
#endif
    {
        // Constructors
        internal ConsoleOutput()
        {
#if !NETCOREAPP
            try
            {
                _consoleHandle = NativeMethods.GetStdHandle(StdOutputHandle);
            }
            catch
            {
                _consoleHandle = IntPtr.Zero;
            }
#endif

            SystemDefaultBackColor = Console.BackgroundColor;
            SystemDefaultForeColor = Console.ForegroundColor;
        }

        // Constants
#if !NETCOREAPP
        private const uint StdOutputHandle = 0xFFFFFFF5;
#endif

        // IVars
#if !NETCOREAPP
        private static IntPtr _consoleHandle;
        private static bool _locallyCreated;
#endif

        // Attributes
        /// <summary>
        /// Specifies the <see cref="ConsoleColor"/> that will be used for the Font of logged entries.
        /// </summary>
        public ConsoleColor ForeColor
        {
            get { return Console.ForegroundColor; }
            set { Console.ForegroundColor = value; }
        }

        /// <summary>
        /// Specifies the <see cref="ConsoleColor"/> that will be used for the Background of logged entries.
        /// </summary>
        public ConsoleColor BackColor
        {
            get { return Console.BackgroundColor; }
            set { Console.BackgroundColor = value; }
        }

        /// <summary>
        /// Specified whenever the messages logged to the console should be auto-colored based on their Severity
        /// </summary>
        public bool AutoColorOutput { get; set; } = true;

        /// <summary>
        /// Stores the default Console's Text Color used by the System. This value is initialized from the currently active Console when the instance is constructed.
        /// </summary>
        public ConsoleColor SystemDefaultForeColor { get; }
        /// <summary>
        /// Stores the default Console's Background Color used by the System. This value is initialized from the currently active Console when the instance is constructed.
        /// </summary>
        public ConsoleColor SystemDefaultBackColor { get; }

        // Methods
        /// <summary>
        /// Writes the specified entry to the <see cref="Console"/>.
        /// </summary>
        /// <exception cref="IOException">Thrown when An I/O error occurred.</exception>
        /// <param name="entry">Text that will be displayed on the <see cref="Console"/></param>
        /// <param name="severity">Severity for the logged message</param>
        /// <remarks>
        /// This method DONT write an end of line to the output.
        /// </remarks>
        public override void Write(string entry, LogLevel severity)
        {
            if (!Enabled)
                return;

            if (AutoColorOutput)
            {
                switch (severity)
                {
                    case LogLevel.Debug:
                        BackColor = ConsoleColor.Black;
                        ForeColor = ConsoleColor.Cyan;
                        break;
                    case LogLevel.Message:
                        BackColor = ConsoleColor.Black;
                        ForeColor = ConsoleColor.White;
                        break;
                    case LogLevel.Warning:
                        BackColor = ConsoleColor.Black;
                        ForeColor = ConsoleColor.DarkYellow;
                        break;
                    case LogLevel.Error:
                        BackColor = ConsoleColor.Black;
                        ForeColor = ConsoleColor.DarkRed;
                        break;
                    case LogLevel.Crash:
                        BackColor = ConsoleColor.DarkRed;
                        ForeColor = ConsoleColor.Black;
                        break;
                }
            }

            Console.Write(entry);
        }

        /// <summary>
        /// Writes the specified entry to the <see cref="Console"/>.
        /// </summary>
        /// <exception cref="IOException">Thrown when An I/O error occurred.</exception>
        /// <param name="entry">Text that will be displayed on the <see cref="Console"/></param>
        /// <remarks>
        /// This method DONT write an end of line to the output.
        /// </remarks>
        public override void Write(string entry)
        {
            Write(entry, LogLevel.Message);
        }


        /// <summary>
        /// Writes the specified entry to the <see cref="Console"/>.
        /// </summary>
        /// <exception cref="IOException">Thrown when An I/O error occurred.</exception>
        /// <param name="entry">Text that will be displayed on the <see cref="Console"/></param>
        /// <param name="severity">Severity for the logged message</param>
        /// <remarks>
        /// This method DO write an end of line to the output.
        /// </remarks>
        public override void WriteLine(string entry, LogLevel severity)
        {
            if (!Enabled)
                return;

            if (AutoColorOutput)
            {
                switch (severity)
                {
                    case LogLevel.Debug:
                        BackColor = ConsoleColor.Black;
                        ForeColor = ConsoleColor.Cyan;
                        break;
                    case LogLevel.Message:
                        BackColor = ConsoleColor.Black;
                        ForeColor = ConsoleColor.White;
                        break;
                    case LogLevel.Warning:
                        BackColor = ConsoleColor.Black;
                        ForeColor = ConsoleColor.DarkYellow;
                        break;
                    case LogLevel.Error:
                        BackColor = ConsoleColor.Black;
                        ForeColor = ConsoleColor.DarkRed;
                        break;
                    case LogLevel.Crash:
                        BackColor = ConsoleColor.DarkRed;
                        ForeColor = ConsoleColor.Black;
                        break;
                }
            }

            Console.WriteLine(entry);
        }

        /// <summary>
        /// Writes the specified entry to the <see cref="Console"/>.
        /// </summary>
        /// <exception cref="IOException">Thrown when An I/O error occurred.</exception>
        /// <param name="entry">Text that will be displayed on the <see cref="Console"/></param>
        /// <remarks>
        /// This method DO write an end of line to the output.
        /// </remarks>
        public override void WriteLine(string entry)
        {
            Write(entry, LogLevel.Message);
        }

        /// <summary>
        /// Writes an end of line to the output.
        /// </summary>
        /// <exception cref="IOException">Thrown when An I/O error occurred.</exception>
        public void WriteLine()
        {
            if (!Enabled)
                return;

            Console.WriteLine();
        }

        /// <summary>
        /// Clears the current line of text on the Console and writes the specified message, using the current ForeColor and BackColor.
        /// </summary>
        /// <exception cref="IOException">Thrown when An I/O error occurred.</exception>
        /// <param name="message">Text that will be displayed on the <see cref="Console"/></param>
        /// <remarks>
        /// If the caret is at the beginning of the line on the Console, it will scroll back to the previous line and replace it.
        /// <para>If instead the caret position is greater than 0, the method will replace the current line on the Console.</para>
        /// </remarks>
        public void Reprint(string message)
        {
            if (!Enabled)
                return;

            if (Console.CursorLeft > 0)
                Console.CursorLeft = 0;
            else
                Console.CursorTop--;

            Console.Write(message);

            if (message == null || message.Length < Console.BufferWidth)
            {
                while (Console.CursorLeft != 0)
                    Console.Write(' ');

                Console.CursorLeft = Console.BufferWidth - 1;
                Console.CursorTop--;
            }
        }

        /// <summary>
        /// Clears the current line of text on the Console.
        /// </summary>
        /// <exception cref="IOException">Thrown when An I/O error occurred.</exception>
        /// <remarks>
        /// If the caret is at the beginning of the line on the Console, it will scroll back to the previous line and replace it.
        /// <para>If instead the caret position is greater than 0, the method will replace the current line on the Console.</para>
        /// </remarks>
        public void Reprint()
        {
            Reprint(null);
        }

        /// <summary>
        /// Clears the entire Console buffer, removing all lines and resetting caret position.
        /// </summary>
        /// <exception cref="IOException">Thrown when An I/O error occurred.</exception>
        public void Clear()
        {
            if (!Enabled)
                return;

            Console.Clear();
        }

#if !NETCOREAPP
        /// <summary>
        /// Creates and attach the current running process to a new Console window.
        /// </summary>
        /// <exception cref="InvalidOperationException">Thrown when the a Console cannot be created and attached to the Application.</exception>
        /// <param name="title">Title that will be displayed in the Title bar for the newly created Console</param>
        /// <remarks>
        /// There can be only ONE console attached to a process.
        /// <para>If a console is already attached to the running process, an exception will be thrown.</para>
        /// </remarks>
        public void CreateConsole(string title)
        {
            // Reset StdOut if redirected. Actually disable it.
            NativeMethods.SetStdHandle(StdOutputHandle, IntPtr.Zero);

            if (NativeMethods.AllocConsole())
                // Should re-enable StdOut internally and set StdOut to the Console Handle.
            {
                //_consoleHandle = NativeMethods.GetStdHandle(StdOutputHandle);

                _consoleHandle = NativeMethods.CreateFile("CONOUT$", 0x40000000, 0x2, IntPtr.Zero, 0x3, 0, IntPtr.Zero);
                SafeFileHandle safeFileHandle = new SafeFileHandle(_consoleHandle, true);
                FileStream fileStream = new FileStream(safeFileHandle, FileAccess.Write);
                Encoding encoding = System.Text.Encoding.GetEncoding(437);
                StreamWriter standardOutput = new StreamWriter(fileStream, encoding);
                standardOutput.AutoFlush = true;
                Console.SetOut(standardOutput);

                if (_consoleHandle != IntPtr.Zero)
                    Console.Title = title;
            }

            if (_consoleHandle == IntPtr.Zero)
            {
                // Failed
                throw new InvalidOperationException("Cannot create and attach a Console to the current Application.");
            }

            _locallyCreated = true;
        }

        /// <summary>
        /// Destroys the console associated with this Application.
        /// </summary>
        public void DestroyConsole()
        {
            if (NativeMethods.FreeConsole())
                _consoleHandle = IntPtr.Zero;
            else
                throw new ObjectDisposedException("There is no console currently associated with this Application.");
        }

        /// <summary>
        /// Checks if there is a console already attached to the current running process.
        /// </summary>
        /// <returns>True if a console is attached to the current running process, otherwise false.</returns>
        public bool HasConsole()
        {
            return _consoleHandle != IntPtr.Zero;
        }

#endif
        // IDisposable Implementation
        /// <summary>
        /// Disposes this instance and frees any used resources.
        /// </summary>
        public void Dispose()
        {
            // Dispose unmanaged resources
            Dispose(true);

            // Suppress Finalization
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Disposes this instance and any managed/unmanaged resources.
        /// </summary>
        /// <param name="disposing">Indicates if the method should dispose Managed resources.</param>
        protected virtual void Dispose(Boolean disposing)
        {
            if (disposing)
            {
                // Managed cleanup
                Enabled = false;

                if (Console.BackgroundColor != SystemDefaultBackColor) Console.BackgroundColor = SystemDefaultBackColor;
                if (Console.ForegroundColor != SystemDefaultForeColor) Console.ForegroundColor = SystemDefaultForeColor;
            }

#if !NETCOREAPP
            // Unmanaged cleanup
            if (HasConsole() && _locallyCreated)
                DestroyConsole();
#endif
        }
    }
}