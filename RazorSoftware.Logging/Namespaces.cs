﻿//! Root namespace for the RazorSoftware Logging library
namespace com.RazorSoftware.Logging { }

//! Contains common endpoints that can be used as Log Message Outputs for the RazorSoftware Logging library
namespace com.RazorSoftware.Logging.Outputs { }
