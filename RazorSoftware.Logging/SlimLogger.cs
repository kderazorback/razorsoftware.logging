﻿/*
    Copyright © 2018 RazorSoftware
    Fabian R. kderazorback@me.com
    This file is part of RazorSoftware Logging Library.

    RazorSoftware Logging Library is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RazorSoftware Logging Library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RazorSoftware Logging Library.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Globalization;
using System.IO;
using System.Text;
using com.RazorSoftware.Logging.Outputs;

namespace com.RazorSoftware.Logging
{
    /// <summary>
    /// Processes Log messages and writes them to its own decentralized File Output which is not managed by the Central <see cref="Log"/> class.
    /// </summary>
    /// <remarks>
    /// This class automatically formats and categorizes logged entries.
    /// </remarks>
#if RAZORSOFTWARE_PUBLIC_API
    public
#else
    internal
#endif
    class SlimLogger : MarshalByRefObject, IDisposable
    {
        // IVars
        private string _lastEntry;
        private int _lastEntryRepeat;
        private FileOutput _output;
        private readonly string _name;

        // Attributes
        /// <summary>
        /// Used to set the indentation of logged entries.
        /// </summary>
        /// <remarks>
        /// For each counter on this attribute, a prefix of three whitespaces will be added to each entry sent to the Output.
        /// </remarks>
        public int IndentLevel { get; private set; }

        /// <summary>
        /// If enabled, multiple messages with the same body and log level will be omitted from the output and instead, a counter will be added with the amount of times the item repeats.
        /// </summary>
        /// <remarks>
        /// This counter appears when a different entry is added to the Output.
        /// </remarks>
        public bool WrapRepetitions { get; set; }

        /// <summary>
        /// Stores the LogLevel of the last entry that was sent to this instance.
        /// </summary>
        /// <remarks>
        /// This is used only for the method <see cref="WriteLine()"/> to prevent adding blank lines for inactive entries (higher LogLevels than the specified).
        /// </remarks>
        public LogLevel LastLogLevel { get; private set; }

        /// <summary>
        /// Stores the amount of error messages that where sent to this instance since its creation.
        /// </summary>
        /// <remarks>This attribute is Arithmetically unchecked for overflows.</remarks>
        public int Errors { get; private set; }

        /// <summary>
        /// Stores the amount of warning messages that where sent to this instance since its creation.
        /// </summary>
        /// <remarks>This attribute is Arithmetically unchecked for overflows.</remarks>
        public int Warnings { get; private set; }

        /// <summary>
        /// Stores the amount of entries that where sent to this instance since its creation.
        /// </summary>
        /// <remarks>This attribute is Arithmetically unchecked for overflows.</remarks>
        public int Entries { get; private set; }

        /// <summary>
        /// Stores the name given to this instance on its Initialization.
        /// </summary>
        /// <remarks>
        /// This attribute is mostly for Application Developers to assign identifiers to each instance of this Class.
        /// </remarks>
        public string Name { get { return _name; } }

        /// <summary>
        /// Stores the <see cref="FileOutput"/> that this instance will use when writting log messages.
        /// </summary>
        public FileOutput Output { get { return _output; } }

        /// <summary>
        /// Enables the instance to print a summary of the number of entries, warnings and errors processed, to the log files when they are being closed
        /// </summary>
        public bool CloseStatistics { get; set; } = true;

        // Constructors
        /// <summary>
        /// Creates a new instance of the SlimLogger class by specifying its Name and output Filename.
        /// </summary>
        /// <param name="filename">Target filename where messages will be logged to.</param>
        /// <param name="name">Name for this new instance of the class.</param>
        public SlimLogger(string filename, string name)
        {
            _name = name;
            Errors = 0;
            Warnings = 0;
            Entries = 0;
            _lastEntry = null;
            _lastEntryRepeat = 0;
            LastLogLevel = LogLevel.Message;
            _output = new FileOutput(filename, name, LogLevel.Message, FileMode.Create);
            _output.Enabled = true;
            _output.AutoFlush = true;
        }
        /// <summary>
        /// Creates a new instance of the SlimLogger class by specifying output Filename only.
        /// </summary>
        /// <param name="filename">Target filename where messages will be logged to.</param>
        public SlimLogger(string filename)
            : this(filename, filename) { }

        // Methods
        /// <summary>
        /// Writes the specified entry to the Output with an specified <see cref="LogLevel"/>.
        /// </summary>
        /// <exception cref="ArgumentException">Thrown when <see cref="Object.ToString()"/> cannot be called for an item inside the elements parameter.</exception>
        /// <param name="entry">Entry that will be logged.</param>
        /// <param name="level"><see cref="LogLevel"/> for the current <paramref name="entry"/>.</param>
        /// <param name="elements">Items that replace the tokens found on the <paramref name="entry"/>.</param>
        /// <remarks>
        /// This method DONT write an End Of Line to the output.
        /// <para>The <paramref name="elements"/> array replaces a list of tokens on the <paramref name="entry"/> string.</para>
        /// <para>These tokens are in the form of: %@ for index-less parameters and %n for indexed parameters where N is a int value ranging from 0 to 9 inclusive.</para>
        ///	<para>To parse each element, the method will call <see cref="Object.ToString()"/> method for the element, If the call fails, an exception is thrown.</para>
        ///	<para>To write the % symbol into the final entry, use %%.</para>
        /// </remarks>
        ///	<example>
        ///	This example shows how to use the tokens on the <paramref name="entry"/> parameter to print the values of the items on the <paramref name="elements"/> array
        ///	<code>
        ///	Write("Item with name %@ costs %@US$. Remember that its name is: %0", LogLevel.Message, "Red Sofa", 79.99f);
        ///	
        /// // This code prints:
        /// //   Item with name Red Sofa costs 79.99US$. Remember that its name is: Red Sofa
        /// </code>
        ///	</example>
        public void Write(string entry, LogLevel level, params object[] elements)
        {
            string expandedString = Log.ExpandStrTokens(entry, elements);
            Write(expandedString, level);
        }

        /// <summary>
        /// Writes the specified entry to the Output with an specified <see cref="LogLevel"/>.
        /// </summary>
        /// <param name="entry">Entry that will be logged.</param>
        /// <param name="level"><see cref="LogLevel"/> for the current entry.</param>
        /// <remarks>
        /// This method DONT write an End Of Line to the output.
        /// </remarks>
        public void Write(string entry, LogLevel level)
        {
            // Default Write() overload
            unchecked
            {
                Entries++;
                if (level > LogLevel.Error)
                    Errors++;
                else
                {
                    if (level > LogLevel.Warning)
                        Warnings++;
                }
            }

            if (level >= Output.MinimumLevel)
                Output.Write(BuildMessageString(entry, level));
        }

        /// <summary>
        /// Writes the specified entry to the Output with a <see cref="LogLevel.Message"/>.
        /// </summary>
        /// <param name="entry">Entry that will be logged.</param>
        /// <remarks>
        /// This method DONT write an End Of Line to the output.
        /// </remarks>
        public void Write(string entry)
        {
            Write(BuildMessageString(entry, LogLevel.Message), LogLevel.Message);
        }

        /// <summary>
        /// Writes the specified entry to the Output with an specified <see cref="LogLevel"/>.
        /// </summary>
        /// <exception cref="ArgumentException">Thrown when <see cref="Object.ToString()"/> cannot be called for an item inside the elements parameter.</exception>
        /// <param name="line">Entry that will be logged.</param>
        /// <param name="level"><see cref="LogLevel"/> for the current <paramref name="line"/>.</param>
        /// <param name="elements">Items that replace the tokens found on the <paramref name="line"/>.</param>
        /// <remarks>
        /// This method DO write an End Of Line to the output.
        /// <para>The <paramref name="elements"/> array replaces a list of tokens on the <paramref name="line"/> string.</para>
        /// <para>These tokens are in the form of: %@ for index-less parameters and %n for indexed parameters where N is a int value ranging from 0 to 9 inclusive.</para>
        ///	<para>To parse each element, the method will call <see cref="Object.ToString()"/> method for the element, If the call fails, an exception is thrown.</para>
        ///	<para>To write the % symbol into the final entry, use %%.</para>
        /// </remarks>
        ///	<example>
        ///	This example shows how to use the tokens on the <paramref name="line"/> parameter to print the values of the items on the <paramref name="elements"/> array
        ///	<code>
        ///	Write("Item with name %@ costs %@US$. Remember that its name is: %0", LogLevel.Message, "Red Sofa", 79.99f);
        ///	
        /// // This code prints:
        /// //   Item with name Red Sofa costs 79.99US$. Remember that its name is: Red Sofa
        /// </code>
        ///	</example>
        public void WriteLine(string line, LogLevel level, params object[] elements)
        {

            string expandedString = Log.ExpandStrTokens(line, elements);
            WriteLine(expandedString, level);
        }

        /// <summary>
        /// Writes the specified entry to the Output with an specified <see cref="LogLevel"/>.
        /// </summary>
        /// <param name="line">Entry that will be logged.</param>
        /// <param name="level"><see cref="LogLevel"/> for the current entry.</param>
        /// <remarks>
        /// This method DO write an End Of Line to the output.
        /// </remarks>
        public void WriteLine(string line, LogLevel level)
        {
            if (level < Output.MinimumLevel)
                return; 

            // Default WriteLine() overload
            unchecked
            {
                Entries++;
                if (level > LogLevel.Error)
                    Errors++;
                else
                {
                    if (level > LogLevel.Warning)
                        Warnings++;
                }
            }

            // If the entry is the same as the previous one (including LogLevel)
            if (String.Equals(line, _lastEntry, StringComparison.OrdinalIgnoreCase) && level == LastLogLevel && !String.IsNullOrEmpty(line) && WrapRepetitions)
            {
                // Increase counter and ignore the entry
                _lastEntryRepeat++;
                return;
            }

            // The strings are not equal or are empty (both)

            if (!String.IsNullOrEmpty(_lastEntry) && _lastEntryRepeat > 0)
                Output.Write("         The last entry repeats " + _lastEntryRepeat.ToString("N0", CultureInfo.CurrentCulture) + " times.\n");

            _lastEntryRepeat = 0;
            _lastEntry = line;
            LastLogLevel = level;

            Output.WriteLine(BuildMessageString(line, level));
        }

        /// <summary>
        /// Writes the specified entry to the Output with a <see cref="LogLevel.Message"/>.
        /// </summary>
        /// <param name="line">Entry that will be logged.</param>
        /// <remarks>
        /// This method DO write an End Of Line to the output.
        /// </remarks>
        public void WriteLine(string line)
        {
            WriteLine(line, LogLevel.Message);
        }

        /// <summary>
        /// Writes a new blank line entry to the Output with an specified <see cref="LogLevel"/>.
        /// </summary>
        /// <param name="level"><see cref="LogLevel"/> for the new blank line.</param>
        public void WriteLine(LogLevel level)
        {
            if (level >= Output.MinimumLevel)
                Output.WriteLine("");
        }

        /// <summary>
        /// Writes a new blank line entry to the Output with the same <see cref="LogLevel"/> as the last entry sent to this instance.
        /// </summary>
        public void WriteLine()
        {
            Output.WriteLine("", LastLogLevel);
        }

        /// <summary>
        /// Increases the automatic entry <see cref="IndentLevel"/> by the specified amount.
        /// </summary>
        /// <param name="value">Amount to increase.</param>
        public void IncreaseIndent(int value)
        {
            IndentLevel += value;
        }

        /// <summary>
        /// Increases the automatic entry <see cref="IndentLevel"/> by one.
        /// </summary>
        public void IncreaseIndent()
        {
            IndentLevel++;
        }

        /// <summary>
        /// Decreases the automatic entry <see cref="IndentLevel"/> by the specified amount.
        /// </summary>
        /// <param name="value">Amount to decrease.</param>
        public void DecreaseIndent(int value)
        {
            IndentLevel -= value;
        }

        /// <summary>
        /// Decreases the automatic entry <see cref="IndentLevel"/> by one.
        /// </summary>
        public void DecreaseIndent()
        {
            IndentLevel--;

            if (IndentLevel < 0)
                IndentLevel = 0;
        }

        /// <summary>
        /// Resets the automatic entry <see cref="IndentLevel"/> to zero.
        /// </summary>
        public void ResetIndent()
        {
            IndentLevel = 0;
        }

        /// <summary>
        /// Builds a formatted log message from an specified entry and a <see cref="LogLevel"/>.
        /// </summary>
        /// <param name="entry">Base entry for the new Message.</param>
        /// <param name="level"><see cref="LogLevel"/> for the new Message.</param>
        /// <returns>A formatted string that represents the full message with its severity tag.</returns>
        private string BuildMessageString(string entry, LogLevel level)
        {
            // Append indentation
            StringBuilder sb = new StringBuilder(new String(' ', IndentLevel * 3));

            // Append LogLevel if its not LogLevel.Message
            if (level != LogLevel.Message)
            {
                if (level == LogLevel.Crash)
                    sb.Append("[CRASH] ");
                else
                {
                    sb.Append(level.ToString());
                    sb.Append(':');
                    sb.Append(' ');
                }
            }

            // Append the entry
            sb.Append(entry);

            // Return the newly created string
            return sb.ToString();
        }

        // IDisposable Implementation
        /// <summary>
        /// Disposes this instance and frees any used resources.
        /// </summary>
        public void Dispose()
        {
            // Dispose unmanaged resources
            Dispose(true);

            // Suppress Finalization
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Disposes this instance and any managed/unmanaged resources.
        /// </summary>
        /// <param name="disposing">Indicates if the method should dispose Managed resources.</param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // Managed cleanup
                if (_lastEntryRepeat > 0)
                    WriteLine(); // Force a Flush of the last entry.

                // Print statistics
                if (CloseStatistics)
                    WriteLine("Logged %@ entries. %@ total Errors. %@ Warnings.", LogLevel.Message, Entries.ToString("N0"), Errors.ToString("N0"), Warnings.ToString("N0"));

                _output.Close();
                _output = null;
            }
        }
    }
}
