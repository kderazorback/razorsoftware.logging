﻿/*
    Copyright © 2018 RazorSoftware
    Fabian R. kderazorback@me.com
    This file is part of RazorSoftware Logging Library.

    RazorSoftware Logging Library is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RazorSoftware Logging Library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RazorSoftware Logging Library.  If not, see <http://www.gnu.org/licenses/>.
 */


using System;
using System.IO;

namespace com.RazorSoftware.Logging.Outputs
{
    /// <summary>
    /// Allows a File to be used as Output for entries sent to the <see cref="Log"/> Class.
    /// </summary>
#if RAZORSOFTWARE_PUBLIC_API
    public class FileOutput : LogOutput
#else
    internal class FileOutput : LogOutput
#endif
    {
        // Constructors
        internal FileOutput(string filename, string identifier, LogLevel minimumLevel, FileMode mode)
        {
            _file = filename;
            _identifier = identifier;
            _newLineFeed = System.Text.Encoding.ASCII.GetBytes(Environment.NewLine);
            MinimumLevel = minimumLevel;

            try { IoStream = new FileStream(filename, mode, FileAccess.Write, FileShare.Read); }
            catch (Exception e)
            { throw new IOException(e.Message); }
            Enabled = true;
        }

        // IVars
        private readonly string _file;
        private readonly string _identifier;
        private readonly byte[] _newLineFeed;

        // Attributes
        /// <summary>
        /// Stores the name of the file where this instance will write entries.
        /// </summary>
        public string File
        {
            get
            {
                return _file;
            }
        }
        /// <summary>
        /// Stores the identifier used as name and passed to this instance when created.
        /// </summary>
        public string Identifier
        {
            get
            {
                return _identifier;
            }
        }
        private FileStream IoStream { get; set; }

        /// <summary>
        /// Indicates if the underlying <see cref="FileStream"/> will be flushed after every write call
        /// </summary>
        public bool AutoFlush { get; set; } = true;

        // Methods
        /// <summary>
        /// Writes the specified entry to the File.
        /// </summary>
        /// <exception cref="IOException">Thrown when the data cannot be written to the opened file.</exception>
        /// <param name="entry">Text that will be written to the File</param>
        /// <remarks>
        /// This method DONT write an end of line to the output.
        /// </remarks>
        public override void Write(string entry)
        {
            if (!Enabled)
                return;

            if (IoStream == null || !IoStream.CanWrite)
                throw new IOException("Cannot write to output stream. Check that the instance was not disposed previously.");

            byte[] buffer = System.Text.Encoding.ASCII.GetBytes(entry);
            IoStream.Write(buffer, 0, buffer.Length);

            if (AutoFlush) Flush();
        }

        /// <summary>
        /// Writes the specified entry to the File.
        /// </summary>
        /// <exception cref="IOException">Thrown when the data cannot be written to the opened file.</exception>
        /// <param name="entry">Text that will be written to the File</param>
        /// <remarks>
        /// This method DO write an end of line to the output.
        /// </remarks>
        public override void WriteLine(string entry)
        {
            if (!Enabled)
                return;

            if (IoStream == null || !IoStream.CanWrite)
                throw new IOException("Cannot write to output stream. Check that the instance was not disposed previously.");

            byte[] buffer = System.Text.Encoding.ASCII.GetBytes(entry);
            IoStream.Write(buffer, 0, buffer.Length);
            IoStream.Write(_newLineFeed, 0, _newLineFeed.Length);

            if (AutoFlush) Flush();
        }

        /// <summary>
        /// Writes an end of line to the output.
        /// <exception cref="IOException">Thrown when the data cannot be written to the opened file.</exception>
        /// </summary>
        public void WriteLine()
        {
            if (!Enabled)
                return;

            if (IoStream == null || !IoStream.CanWrite)
                throw new IOException("Cannot write to output stream. Check that the instance was not disposed previously.");

            IoStream.Write(_newLineFeed, 0, _newLineFeed.Length);

            if (AutoFlush) Flush();
        }

        /// <summary>
        /// Flushes the underlying stream, wirtting any buffered data to disk
        /// </summary>
        public void Flush()
        {
            if (IoStream == null || !IoStream.CanWrite)
                throw new ObjectDisposedException("This Instance has been already disposed.");

            IoStream.Flush();
        }

        /// <summary>
        /// Closes the previously opened file.
        /// </summary>
        /// <exception cref="ObjectDisposedException">Thrown when the instance has already been shut down.</exception>
        public void Close()
        {
            if (IoStream == null || !IoStream.CanWrite)
                throw new ObjectDisposedException("This Instance has been already disposed.");

            IoStream.Flush();
            IoStream.Close();
            IoStream.Dispose();
            IoStream = null;
            Enabled = false;
        }
    }
}
