﻿/*
    Copyright © 2018 RazorSoftware
    Fabian R. kderazorback@me.com
    This file is part of RazorSoftware Logging Library.

    RazorSoftware Logging Library is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RazorSoftware Logging Library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RazorSoftware Logging Library.  If not, see <http://www.gnu.org/licenses/>.
 */


using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading;
using System.IO;
using System.Reflection;
using com.RazorSoftware.Logging.Outputs;
using System.Globalization;

namespace com.RazorSoftware.Logging
{
    /// <summary>
    /// Logs messages generated from various parts of the Application to multiple configurable outputs.
    /// </summary>
    /// <remarks>
    /// All methods in this class used for logging entries are Thread-Safe and actually locks outputs when a message is received.
    /// <para>Its recommended to use the <see cref="Logger"/> class for writting to this class.</para>
    /// <para>The host is responsible for releasing and disposing any Custom Objects registered on this Class</para>
    /// </remarks>   
#if RAZORSOFTWARE_PUBLIC_API
    public
#else
    internal
#endif
    static class Log
    {
        // IVars
        private static bool _suppressSecurity;
        private static bool _useLocalSecurity;
        private static List<FileOutput> _files;
        private static List<IOutput> _customOutputs;
        private static string _lastWriteLineString = "";
        private static int _lastWriteLineStringRepeats;
        private static bool _pauseAutoWrapping = false;

        // Attributes
        /// <summary>
        /// Enables or Disables the entire Logging functionality (including all its outputs)
        /// </summary>
        public static bool Enabled { get; set; }

        /// <summary>
        /// Store files that will be used as outputs for logged messages.
        /// </summary>
        /// <remarks>
        /// If no files are specified, File logging will be disabled.
        /// </remarks>
        public static ReadOnlyCollection<FileOutput> Files
        {
            get { return _files.AsReadOnly(); }
        }
        /// <summary>
        /// Stores a console instance that will be used as output for logged messages.
        /// </summary>
        /// <remarks>
        /// Only one Console Output can be active at a time.
        /// </remarks>
        public static ConsoleOutput Console { get; private set; }
        /// <summary>
        /// Stores a debugger instance that will be used as output for logged messages.
        /// </summary>
        /// <remarks>
        /// Only one Debugger Output can be active at a time.
        /// </remarks>
        public static DebuggerOutput Debugger { get; private set; }
        /// <summary>
        /// Stores the amount of error messages that where sent to the Log since its initialization.
        /// </summary>
        /// <remarks>This attribute is Arithmetically unchecked for overflows.</remarks>
        public static int Errors { get; private set; }
        /// <summary>
        /// Stores the amount of warning messages that where sent to the Log since its initialization.
        /// </summary>
        /// <remarks>This attribute is Arithmetically unchecked for overflows.</remarks>
        public static int Warnings { get; private set; }
        /// <summary>
        /// Stores the amount of entries that where sent to the Log since its initialization.
        /// </summary>
        /// <remarks>This attribute is Arithmetically unchecked for overflows.</remarks>
        public static int Entries { get; private set; }

        /// <summary>
        /// Used by the class to Lock access to outputs and provide Thread Safety for Interprocess communication.
        /// </summary>
        private static Mutex AccessLock { get; set; }
        /// <summary>
        /// Disables entirely Thread-Safety in this class.
        /// </summary>
        /// <exception cref="InvalidOperationException">Thrown when this attribute is being modified when the Instance is already Initialized.</exception>
        /// <remarks>
        /// This could lead to malformed entries being written to the outputs.
        /// <para>This attribute cannot be modified once the instance is Initialized.</para>
        /// </remarks>
        public static bool SuppressSecurity
        {
            get
            {
                return _suppressSecurity;
            }
            set
            {
                if (AccessLock != null)
                    throw new InvalidOperationException("This attribute is ReadOnly once the instance is initialized.");

                _suppressSecurity = value;
            }
        }
        /// <summary>
        /// When this attribute is set, a more lightweight and fast thread-locking primitive will be used for managing multiple access to the outputs, which will improve the Log overall performance, but will cause issues and unexpected exceptions when using multiple <see cref="AppDomain"/>.
        /// </summary>
        /// <exception cref="InvalidOperationException">Thrown when this attribute is being modified when the Instance is already Initialized.</exception>
        /// <remarks>
        /// This attribute cannot be modified once the instance is Initialized.
        /// </remarks>
        public static bool UseLocalSecurity
        {
            get
            {
                return _useLocalSecurity;
            }
            set
            {
                if (AccessLock != null)
                    throw new InvalidOperationException("This attribute is ReadOnly once the instance is initialized.");

                _useLocalSecurity = value;
            }
        }
        /// <summary>
        /// Stores the Application Name that will be displayed on Outputs headers.
        /// </summary>
        /// <remarks>
        /// If this value is Null or an Empty String when the instance Initializes, a default name will be extracted from the current Assembly Manifest.
        /// </remarks>
        public static string AppName { get; set; }
        /// <summary>
        /// Stores the Application Version that will be logged to the Outputs.
        /// </summary>
        /// <remarks>
        /// If this value is Null or an Empty String when the instance Initializes, a default version will be extracted from the current Assembly Manifest
        /// </remarks>
        public static string AppVersion { get; set; }
        /// <summary>
        /// Stores a list of Custom objects that will be used as Outputs for logged Messages
        /// </summary>
        public static ReadOnlyCollection<IOutput> CustomOutputs
        {
            get { return _customOutputs.AsReadOnly(); }
        }
        /// <summary>
        /// Enables the AutoWrapping functionality for all lines written to Outputs. When this feature is enabled, repetitions for a single message will be ignored and will not appear on any output until a new message is written.
        /// When a new message is sent, this class will automatically print a summary of how many times the first message appeared before writting the second one.
        /// This summary is printed by using the template from <see cref="AutoWrappingReportString"/> and by replacing its single parameter with the repetition count. The loglevel of this message can be specified on the <see cref="AutoWrappingReportLogLevel"/> property.
        /// When this feature is enabled, you must call <see cref="Shutdown"/> before closing any outputs to avoid missing any remaining summary message.
        /// </summary>
        public static bool AutoWrappingEnabled { get; set; }
        /// <summary>
        /// Specifies the <see cref="LogLevel"/> of the report message written for string repetitions, when the <see cref="AutoWrappingEnabled"/> feature is enabled.
        /// </summary>
        public static LogLevel AutoWrappingReportLogLevel { get; set; } = LogLevel.Message;
        /// <summary>
        /// Specifies the string template to be used when printing summaries for string repetitions, when the <see cref="AutoWrappingEnabled"/> feature is enabled.
        /// This string is expanded internally and can contain a single token or placeholder in the form of "%@" or "%1" for printing the Repetition count.
        /// </summary>
        public static string AutoWrappingReportString { get; set; } = "The previous line repeats %@ more times.";
        /// <summary>
        /// Enables the instance to print a summary of the number of entries, warnings and errors processed, to the log files when they are being closed
        /// </summary>
        public static bool CloseStatistics { get; set; } = true;
        /// <summary>
        /// Returns a value indicating if the Library has been already initialized by a call to <see cref="Initialize"/>
        /// </summary>
        public static bool IsInitialized { get; private set; } = false;


        // Methods
        /// <summary>
        /// Opens the specified file as an output for logged messages, applying the specified <see cref="LogLevel"/> as filter.
        /// </summary>
        /// <exception cref="System.IO.FileNotFoundException">Thrown when the path to the target file cannot be found, or if <paramref name="append"/> is set and the file doesnt exists.</exception>
        /// <exception cref="System.IO.IOException">Thrown when the target file cannot be created or opened for Writting.</exception>
        /// <param name="fileName">Path to the file that will be opened.</param>
        /// <param name="identifier">This identifier will be used to name the File as an Output.</param>
        /// <param name="level">Minimum Logging Level that the entries must have to be logged on this newly created Output.</param>
        /// <param name="append">If append parameter is set, then the logger will seek to the end of the file after opening it and append all new logged messages to the end. If the parameter is not set, the file will be erased when opened.</param>
        public static void OpenFile(string fileName, string identifier, LogLevel level, bool append)
        {
            if ('/' == Path.DirectorySeparatorChar)
                fileName = fileName.Replace('\\', Path.DirectorySeparatorChar);

            FileInfo fi = new FileInfo(fileName);
            if (!fi.Directory.Exists)
                fi.Directory.Create();

            FileOutput output = new FileOutput(fi.FullName, identifier, level, (append ? FileMode.Append : FileMode.Create));

            // Write header information
            output.WriteLine(AppName + " Logging Output");
            output.WriteLine("Version: " + AppVersion);
            output.WriteLine("=========================================================================");
            output.WriteLine();
            output.WriteLine("   LogLevel: " + output.MinimumLevel);
            output.WriteLine("   Identifier: " + output.Identifier);
            output.WriteLine("   Timestamp: " + DateTime.Now.ToString(System.Globalization.CultureInfo.InvariantCulture));
            output.WriteLine();
            output.WriteLine();

            AcquireLock(_files);
            _files.Add(output);
            ReleaseLock(_files);
        }

        /// <summary>
        /// Appends footer data to the specified previously opened file and then closes it.
        /// </summary>
        /// <exception cref="KeyNotFoundException">Thrown when the file with the specified identifier cannot be found.</exception>
        /// <exception cref="IOException">Thrown when file cannot be closed.</exception>
        /// <param name="identifier">Identifier of the file to be closed.</param>
        public static void CloseFile(string identifier)
        {
            AcquireLock(_files);
            for (int i = 0; i < _files.Count; i++)
            {
                if (String.Equals(_files[i].Identifier, identifier, StringComparison.OrdinalIgnoreCase))
                {
                    FileOutput instance = _files[i];
                    _files.RemoveAt(i);
                    ReleaseLock(_files);

                    // Append footer data to the Output
                    instance.WriteLine();
                    instance.WriteLine();
                    instance.WriteLine("=========================================================================");
                    instance.WriteLine("Timestamp: " + DateTime.Now.ToString(System.Globalization.CultureInfo.InvariantCulture));
                    instance.WriteLine();
                    instance.WriteLine("   LOG CLOSED");

                    // Close File Output
                    instance.Close();
                    return;
                }
            }

            ReleaseLock(_files);

            throw new KeyNotFoundException("The identifier cannot be found on the collection.");
        }

        /// <summary>
        /// Appends footer data to the specified previously opened file (by index) and then closes it.
        /// </summary>
        /// <exception cref="IndexOutOfRangeException">Thrown when the index supplied is outside the limits of the outputs array.</exception>
        /// <exception cref="IOException">Thrown when file cannot be closed.</exception>
        /// <param name="index">Index of the File output to be closed.</param>
        public static void CloseFile(int index)
        {
            AcquireLock(_files);

            FileOutput instance;
            try
            {
                instance = _files[index];
                _files.RemoveAt(index);
            }
            finally
            { ReleaseLock(_files); }
            
            // Append footer data to the Output
            instance.WriteLine();
            instance.WriteLine();
            instance.WriteLine("=========================================================================");
            instance.WriteLine("Timestamp: " + DateTime.Now.ToString(System.Globalization.CultureInfo.InvariantCulture));
            instance.WriteLine();
            instance.WriteLine("   LOG CLOSED");

            // Close File Output
            instance.Close();
        }

        /// <summary>
        /// Registers a Custom object for receiving messages logged via this class
        /// </summary>
        /// <param name="output">Custom output that will be Attached</param>
        /// <exception cref="ArgumentNullException">Thrown when the output argument is Null</exception>
        /// <exception cref="ArgumentException">Thrown when the specified object is already attached to this Class, or there is another object with the same Identifier already registered</exception>
        public static void AttachOutput(IOutput output)
        {
            if (output == null)
                throw new ArgumentNullException("output");

            foreach (IOutput sel in _customOutputs)
            {
                if (sel == output)
                    throw new ArgumentException("The specified object is already registered on this Class");

                if (string.Equals(sel.Identifier, output.Identifier, StringComparison.OrdinalIgnoreCase))
                    throw new ArgumentException("There is another object with the same Identifier already registered on this Class");
            }

            _customOutputs.Add(output);
        }

        /// <summary>
        /// Frees a reference to a previously attached Object, and stops logging messages to it
        /// </summary>
        /// <param name="output">Target output that will be Detached</param>
        /// <exception cref="ArgumentNullException">Thrown when the output argument is Null</exception>
        /// <exception cref="KeyNotFoundException">Thrown when the specified object is not registered on this Class</exception>
        public static void DetachOutput(IOutput output)
        {
            if (output == null)
                throw new ArgumentNullException("output");

            foreach (IOutput sel in _customOutputs)
            {
                if (sel == output)
                {
                    _customOutputs.Remove(sel);
                    return;
                }
            }

            throw new KeyNotFoundException("The specified object is not registered on this Class");
        }

        /// <summary>
        /// Frees a reference to a previously attached Object that matches the specified Identifier, and stops logging messages to it
        /// </summary>
        /// <param name="identifier">Target output that will be Detached</param>
        /// <exception cref="ArgumentNullException">Thrown when the Identifier argument is Null</exception>
        /// <exception cref="KeyNotFoundException">Thrown when the specified object is not registered on this Class</exception>
        public static void DetachOutput(string identifier)
        {
            if (string.IsNullOrEmpty(identifier))
                throw new ArgumentNullException("identifier");

            foreach (IOutput output in _customOutputs)
            {
                if (string.Equals(output.Identifier, identifier, StringComparison.OrdinalIgnoreCase))
                {
                    _customOutputs.Remove(output);
                    return;
                }
            }

            throw new KeyNotFoundException("There is no object registered on this Class with the specified Identifier");
        }

        /// <summary>
        /// Frees all custom objects attached to this Class that are being used as logging Outputs
        /// </summary>
        public static void DetachOutputs()
        {
            _customOutputs.Clear();
        }

        /// <summary>
        /// Writes the specified entry into all outputs with a higher or equal <see cref="LogLevel"/>. This variant replaces tokens in the form of %@ or %i (one-based) with the arguments provided in <paramref name="elements"/>>.
        /// </summary>
        /// <exception cref="ArgumentException">Thrown when <see cref="Object.ToString()"/> cannot be called for an item inside the elements parameter.</exception>
        /// <exception cref="IOException">Thrown when the entry cannot be written to an Output.</exception>
        /// <param name="entry">Entry that will be logged.</param>
        /// <param name="level"><see cref="LogLevel"/> for the current <paramref name="entry"/>.</param>
        /// <param name="elements">Items that replace the tokens found on the <paramref name="entry"/>.</param>
        /// <remarks>
        /// This method DONT write an End Of Line to the outputs.
        /// <para>The <paramref name="elements"/> array replaces a list of tokens on the <paramref name="entry"/> string.</para>
        /// <para>These tokens are in the form of: %@ for index-less parameters and %n for indexed parameters where N is a int value.</para>
        ///	<para>To parse each element, the method will call <see cref="Object.ToString()"/> method for the element, If the call fails, an exception is thrown.</para>
        ///	<para>To write the % symbol into the final entry, use %%.</para>
        /// </remarks>
        ///	<example>
        ///	This example shows how to use the tokens on the <paramref name="entry"/> parameter to print the values of the items on the <paramref name="elements"/> array
        ///	<code>
        ///	Write("Item with name %@ costs %@US$. Remember that its name is: %1", LogLevel.Message, "Red Sofa", 79.99f);
        ///	
        /// // This code prints:
        /// //   Item with name Red Sofa costs 79.99US$. Remember that its name is: Red Sofa
        /// </code>
        ///	</example>
        public static void Write(string entry, LogLevel level, params object[] elements)
        {
            if (!Enabled)
                return;

            if (String.IsNullOrEmpty(entry))
                return;

            // Write the resulting string to the default overload
            Write(ExpandStrTokens(entry, elements), level);
        }

        /// <summary>
        /// Writes the specified entry into all outputs with a higher or equal <see cref="LogLevel"/>.
        /// </summary>
        /// <exception cref="IOException">Thrown when the entry cannot be written to an Output.</exception>
        /// <param name="entry">Entry that will be logged.</param>
        /// <param name="level"><see cref="LogLevel"/> for the current entry.</param>
        /// <remarks>
        /// This method DONT write an End Of Line to the outputs.
        /// </remarks>
        public static void Write(string entry, LogLevel level)
        {
            if (!Enabled)
                return;

            unchecked
            {
                Entries++;
                if (level > LogLevel.Error)
                    Errors++;
                else
                {
                    if (level > LogLevel.Warning)
                        Warnings++;
                }
            }

            // Write to the Console
            if (Console.Enabled && level >= Console.MinimumLevel)
            {
                AcquireLock(Console);
                Console.Write(entry, level);
                ReleaseLock(Console);
            }

            // Write to the Debugger
            if (Debugger.Enabled && level >= Debugger.MinimumLevel)
            {
                AcquireLock(Debugger);
                Debugger.Write(entry);
                ReleaseLock(Debugger);
            }

            // Write to each File
            if (_files.Count > 0)
            {
                try
                {
                    AcquireLock(_files);
                    foreach (FileOutput f in _files)
                    {
                        if (f.Enabled && level >= f.MinimumLevel)
                            f.Write(entry);
                    }
                }
                finally { ReleaseLock(_files); }
            }

            // Write to Custom Outputs
            if (_customOutputs.Count > 0)
            {
                try
                {
                    AcquireLock(_customOutputs);
                    foreach (IOutput output in _customOutputs)
                    {
                        if (output.Enabled && level >= output.MinimumLevel)
                            output.Write(entry, level);
                    }
                }
                finally { ReleaseLock(_customOutputs); }
            }
        }

        /// <summary>
        /// Writes the specified entry into all outputs with a <see cref="LogLevel"/> of <see cref="LogLevel.Message"/> or higher.
        /// </summary>
        /// <exception cref="IOException">Thrown when the entry cannot be written to an Output.</exception>
        /// <param name="entry">Entry that will be logged.</param>
        /// <remarks>
        /// This method DONT write an End Of Line to the outputs.
        /// </remarks>
        public static void Write(string entry)
        {
            Write(entry, LogLevel.Message);
        }

        /// <summary>
        /// Writes the specified entry into all outputs with a higher or equal <see cref="LogLevel"/> using the specified Background and Font colors. This variant replaces tokens in the form of %@ or %i (one-based) with the arguments provided in <paramref name="elements"/>>.
        /// </summary>
        /// <exception cref="ArgumentException">Thrown when <see cref="Object.ToString()"/> cannot be called for an item inside the elements parameter.</exception>
        /// <exception cref="IOException">Thrown when the entry cannot be written to an Output.</exception>
        /// <param name="entry">Entry that will be logged.</param>
        /// <param name="background">Background color used to display the message</param>
        /// <param name="foreground">Font color used to display the message</param>
        /// <param name="level"><see cref="LogLevel"/> for the current <paramref name="entry"/>.</param>
        /// <param name="elements">Items that replace the tokens found on the <paramref name="entry"/>.</param>
        /// <remarks>
        /// This method DONT write an End Of Line to the outputs.
        /// <para>The <paramref name="elements"/> array replaces a list of tokens on the <paramref name="entry"/> string.</para>
        /// <para>These tokens are in the form of: %@ for index-less parameters and %n for indexed parameters where N is a int value.</para>
        ///	<para>To parse each element, the method will call <see cref="Object.ToString()"/> method for the element, If the call fails, an exception is thrown.</para>
        ///	<para>To write the % symbol into the final entry, use %%.</para>
        /// </remarks>
        ///	<example>
        ///	This example shows how to use the tokens on the <paramref name="entry"/> parameter to print the values of the items on the <paramref name="elements"/> array
        ///	<code>
        ///	Write("Item with name %@ costs %@US$. Remember that its name is: %1", LogLevel.Message, "Red Sofa", 79.99f);
        ///	
        /// // This code prints:
        /// //   Item with name Red Sofa costs 79.99US$. Remember that its name is: Red Sofa
        /// </code>
        ///	</example>
        public static void WriteColored(string entry, ConsoleColor background, ConsoleColor foreground, LogLevel level, params object[] elements)
        {
            if (Console.Enabled)
            {
                ConsoleColor back = Console.BackColor;
                ConsoleColor fore = Console.ForeColor;
                bool autoColor = Console.AutoColorOutput;
                Console.BackColor = background;
                Console.ForeColor = foreground;
                Console.AutoColorOutput = false;
                Write(entry, level, elements);
                Console.AutoColorOutput = autoColor;
                Console.BackColor = back;
                Console.ForeColor = fore;
            }
            else
                Write(entry, level, elements);
        }

        /// <summary>
        /// Writes the specified entry into all outputs with a higher or equal <see cref="LogLevel"/> using the specified Font color. This variant replaces tokens in the form of %@ or %i (one-based) with the arguments provided in <paramref name="elements"/>>.
        /// </summary>
        /// <exception cref="ArgumentException">Thrown when <see cref="Object.ToString()"/> cannot be called for an item inside the elements parameter.</exception>
        /// <exception cref="IOException">Thrown when the entry cannot be written to an Output.</exception>
        /// <param name="entry">Entry that will be logged.</param>
        /// <param name="foreground">Font color used to display the message</param>
        /// <param name="level"><see cref="LogLevel"/> for the current <paramref name="entry"/>.</param>
        /// <param name="elements">Items that replace the tokens found on the <paramref name="entry"/>.</param>
        /// <remarks>
        /// This method DONT write an End Of Line to the outputs.
        /// <para>The <paramref name="elements"/> array replaces a list of tokens on the <paramref name="entry"/> string.</para>
        /// <para>These tokens are in the form of: %@ for index-less parameters and %n for indexed parameters where N is a int value.</para>
        ///	<para>To parse each element, the method will call <see cref="Object.ToString()"/> method for the element, If the call fails, an exception is thrown.</para>
        ///	<para>To write the % symbol into the final entry, use %%.</para>
        /// </remarks>
        ///	<example>
        ///	This example shows how to use the tokens on the <paramref name="entry"/> parameter to print the values of the items on the <paramref name="elements"/> array
        ///	<code>
        ///	Write("Item with name %@ costs %@US$. Remember that its name is: %1", LogLevel.Message, "Red Sofa", 79.99f);
        ///	
        /// // This code prints:
        /// //   Item with name Red Sofa costs 79.99US$. Remember that its name is: Red Sofa
        /// </code>
        ///	</example>
        public static void WriteColored(string entry, ConsoleColor foreground, LogLevel level, params object[] elements)
        {
            if (Console.Enabled)
            {
                ConsoleColor fore = Console.ForeColor;
                bool autoColor = Console.AutoColorOutput;
                Console.ForeColor = foreground;
                Console.AutoColorOutput = false;
                Write(entry, level, elements);
                Console.AutoColorOutput = autoColor;
                Console.ForeColor = fore;
            }
            else
                Write(entry, level, elements);
        }

        /// <summary>
        /// Writes the specified entry into all outputs with a higher or equal <see cref="LogLevel"/> with the specified Background and Font colors.
        /// </summary>
        /// <exception cref="IOException">Thrown when the entry cannot be written to an Output.</exception>
        /// <param name="entry">Entry that will be logged.</param>
        /// <param name="background">Background color used to display the message</param>
        /// <param name="foreground">Font color used to display the message</param>
        /// <param name="level"><see cref="LogLevel"/> for the current entry.</param>
        /// <remarks>
        /// This method DONT write an End Of Line to the outputs.
        /// </remarks>
        public static void WriteColored(string entry, ConsoleColor background, ConsoleColor foreground, LogLevel level = LogLevel.Message)
        {
            WriteColored(entry, background, foreground, level, null);
        }

        /// <summary>
        /// Writes the specified entry into all outputs with a higher or equal <see cref="LogLevel"/> with the specified Font color.
        /// </summary>
        /// <exception cref="IOException">Thrown when the entry cannot be written to an Output.</exception>
        /// <param name="entry">Entry that will be logged.</param>
        /// <param name="foreground">Font color used to display the message</param>
        /// <param name="level"><see cref="LogLevel"/> for the current entry.</param>
        /// <remarks>
        /// This method DONT write an End Of Line to the outputs.
        /// </remarks>
        public static void WriteColored(string entry, ConsoleColor foreground, LogLevel level = LogLevel.Message)
        {
            WriteColored(entry, foreground, level, null);
        }

        /// <summary>
        /// Writes the specified entry into all outputs with a higher or equal <see cref="LogLevel"/>. This variant replaces tokens in the form of %@ or %i (one-based) with the arguments provided in <paramref name="elements"/>>.
        /// </summary>
        /// <exception cref="ArgumentException">Thrown when <see cref="Object.ToString()"/> cannot be called for an item inside the elements parameter.</exception>
        /// <exception cref="IOException">Thrown when the entry cannot be written to an Output.</exception>
        /// <param name="line">Entry that will be logged.</param>
        /// <param name="level"><see cref="LogLevel"/> for the current <paramref name="line"/>.</param>
        /// <param name="elements">Items that replace the tokens found on the <paramref name="line"/>.</param>
        /// <remarks>
        /// This method DO write an End Of Line to the outputs.
        /// <para>The <paramref name="elements"/> array replaces a list of tokens on the <paramref name="line"/> string.</para>
        /// <para>These tokens are in the form of: %@ for index-less parameters and %n for indexed parameters where N is a int value.</para>
        ///	<para>To parse each element, the method will call <see cref="Object.ToString()"/> method for the element, If the call fails, an exception is thrown.</para>
        ///	<para>To write the % symbol into the final entry, use %%.</para>
        /// </remarks>
        ///	<example>
        ///	This example shows how to use the tokens on the <paramref name="line"/> parameter to print the values of the items on the <paramref name="elements"/> array
        ///	<code>
        ///	Write("Item with name %@ costs %@US$. Remember that its name is: %1", LogLevel.Message, "Red Sofa", 79.99f);
        ///	
        /// // This code prints:
        /// //   Item with name Red Sofa costs 79.99US$. Remember that its name is: Red Sofa
        /// </code>
        ///	</example>
        public static void WriteLine(string line, LogLevel level, params object[] elements)
        {
            if (!Enabled)
                return;

            if (String.IsNullOrEmpty(line))
            {
                WriteLine(level);
                return;
            }

            // Write the resulting string to the default overload
            WriteLine(ExpandStrTokens(line, elements), level);
        }

        /// <summary>
        /// Writes the specified entry into all outputs with a higher or equal <see cref="LogLevel"/>.
        /// </summary>
        /// <param name="line">Entry that will be logged.</param>
        /// <param name="level"><see cref="LogLevel"/> for the current entry.</param>
        /// <remarks>
        /// This method DO write an End Of Line to the outputs.
        /// </remarks>
        public static void WriteLine(string line, LogLevel level)
        {
            if (!Enabled)
                return;

            unchecked
            {
                Entries++;
                if (level > LogLevel.Error)
                    Errors++;
                else
                {
                    if (level > LogLevel.Warning)
                        Warnings++;
                }
            }

            if (AutoWrappingEnabled && !_pauseAutoWrapping)
            {
                if (string.Equals(line, _lastWriteLineString, StringComparison.Ordinal) && _lastWriteLineStringRepeats < int.MaxValue)
                {
                    _lastWriteLineStringRepeats++;
                    return;
                }
                else
                {
                    if (_lastWriteLineStringRepeats > 0)
                    {
                        _pauseAutoWrapping = true;
                        WriteLine(ExpandStrTokens(AutoWrappingReportString, _lastWriteLineStringRepeats.ToString("N0")), AutoWrappingReportLogLevel);
                        _pauseAutoWrapping = false;
                    }

                    _lastWriteLineString = line;
                    _lastWriteLineStringRepeats = 0;
                }
            }

            // Write to the Console
            if (Console.Enabled && level >= Console.MinimumLevel)
            {
                AcquireLock(Console);
                Console.WriteLine(line, level);
                ReleaseLock(Console);
            }

            // Write to the Debugger
            if (Debugger.Enabled && level >= Debugger.MinimumLevel)
            {
                AcquireLock(Debugger);
                Debugger.WriteLine(line);
                ReleaseLock(Debugger);
            }

            // Write to each File
            if (_files.Count > 0)
            {
                try
                {
                    AcquireLock(_files);
                    foreach (FileOutput f in _files)
                    {
                        if (f.Enabled && level >= f.MinimumLevel)
                            f.WriteLine(line);
                    }
                }
                finally { ReleaseLock(_files); }
            }

            // Write to Custom Outputs
            if (_customOutputs.Count > 0)
            {
                try
                {
                    AcquireLock(_customOutputs);
                    foreach (IOutput output in _customOutputs)
                    {
                        if (output.Enabled && level >= output.MinimumLevel)
                            output.WriteLine(line, level);
                    }
                }
                finally { ReleaseLock(_customOutputs); }
            }
        }

        /// <summary>
        /// Writes the specified entry into all outputs with a <see cref="LogLevel"/> of <see cref="LogLevel.Message"/> or higher.
        /// </summary>
        /// <param name="line">Entry that will be logged.</param>
        /// <remarks>
        /// This method DO write an End Of Line to the outputs.
        /// </remarks>
        public static void WriteLine(string line)
        {
            WriteLine(line, LogLevel.Message);
        }

        /// <summary>
        /// Writes a new blank line entry into all outputs with a higher or equal <see cref="LogLevel"/>.
        /// </summary>
        /// <param name="level"><see cref="LogLevel"/> for the new blank line.</param>
        public static void WriteLine(LogLevel level)
        {
            WriteLine("", level);
        }

        /// <summary>
        /// Writes a new blank line entry into the outputs with a <see cref="LogLevel"/> of <see cref="LogLevel.Message"/>.
        /// </summary>
        public static void WriteLine()
        {
            WriteLine(LogLevel.Message);
        }

        /// <summary>
        /// Writes the specified entry into all outputs with a higher or equal <see cref="LogLevel"/> using the specified Background and Font colors. This variant replaces tokens in the form of %@ or %i (one-based) with the arguments provided in <paramref name="elements"/>>.
        /// </summary>
        /// <exception cref="ArgumentException">Thrown when <see cref="Object.ToString()"/> cannot be called for an item inside the elements parameter.</exception>
        /// <exception cref="IOException">Thrown when the entry cannot be written to an Output.</exception>
        /// <param name="line">Entry that will be logged.</param>
        /// <param name="background">Background color used to display the message</param>
        /// <param name="foreground">Font color used to display the message</param>
        /// <param name="level"><see cref="LogLevel"/> for the current <paramref name="line"/>.</param>
        /// <param name="elements">Items that replace the tokens found on the <paramref name="line"/>.</param>
        /// <remarks>
        /// This method DO write an End Of Line to the outputs.
        /// <para>The <paramref name="elements"/> array replaces a list of tokens on the <paramref name="line"/> string.</para>
        /// <para>These tokens are in the form of: %@ for index-less parameters and %n for indexed parameters where N is a int value.</para>
        ///	<para>To parse each element, the method will call <see cref="Object.ToString()"/> method for the element, If the call fails, an exception is thrown.</para>
        ///	<para>To write the % symbol into the final entry, use %%.</para>
        /// </remarks>
        ///	<example>
        ///	This example shows how to use the tokens on the <paramref name="line"/> parameter to print the values of the items on the <paramref name="elements"/> array
        ///	<code>
        ///	Write("Item with name %@ costs %@US$. Remember that its name is: %1", LogLevel.Message, "Red Sofa", 79.99f);
        ///	
        /// // This code prints:
        /// //   Item with name Red Sofa costs 79.99US$. Remember that its name is: Red Sofa
        /// </code>
        ///	</example>
        public static void WriteColoredLine(string line, ConsoleColor background, ConsoleColor foreground, LogLevel level, params object[] elements)
        {
            if (Console.Enabled)
            {
                ConsoleColor back = Console.BackColor;
                ConsoleColor fore = Console.ForeColor;
                bool autoColor = Console.AutoColorOutput;
                Console.BackColor = background;
                Console.ForeColor = foreground;
                Console.AutoColorOutput = false;
                WriteLine(line, level, elements);
                Console.AutoColorOutput = autoColor;
                Console.BackColor = back;
                Console.ForeColor = fore;
            }
            else
                WriteLine(line, level, elements);
        }

        /// <summary>
        /// Writes the specified entry into all outputs with a higher or equal <see cref="LogLevel"/> using the specified Font color. This variant replaces tokens in the form of %@ or %i (one-based) with the arguments provided in <paramref name="elements"/>>.
        /// </summary>
        /// <exception cref="ArgumentException">Thrown when <see cref="Object.ToString()"/> cannot be called for an item inside the elements parameter.</exception>
        /// <exception cref="IOException">Thrown when the entry cannot be written to an Output.</exception>
        /// <param name="line">Entry that will be logged.</param>
        /// <param name="foreground">Font color used to display the message</param>
        /// <param name="level"><see cref="LogLevel"/> for the current <paramref name="line"/>.</param>
        /// <param name="elements">Items that replace the tokens found on the <paramref name="line"/>.</param>
        /// <remarks>
        /// This method DO write an End Of Line to the outputs.
        /// <para>The <paramref name="elements"/> array replaces a list of tokens on the <paramref name="line"/> string.</para>
        /// <para>These tokens are in the form of: %@ for index-less parameters and %n for indexed parameters where N is a int value.</para>
        ///	<para>To parse each element, the method will call <see cref="Object.ToString()"/> method for the element, If the call fails, an exception is thrown.</para>
        ///	<para>To write the % symbol into the final entry, use %%.</para>
        /// </remarks>
        ///	<example>
        ///	This example shows how to use the tokens on the <paramref name="line"/> parameter to print the values of the items on the <paramref name="elements"/> array
        ///	<code>
        ///	Write("Item with name %@ costs %@US$. Remember that its name is: %1", LogLevel.Message, "Red Sofa", 79.99f);
        ///	
        /// // This code prints:
        /// //   Item with name Red Sofa costs 79.99US$. Remember that its name is: Red Sofa
        /// </code>
        ///	</example>
        public static void WriteColoredLine(string line, ConsoleColor foreground, LogLevel level, params object[] elements)
        {
            if (Console.Enabled)
            {
                ConsoleColor fore = Console.ForeColor;
                bool autoColor = Console.AutoColorOutput;
                Console.ForeColor = foreground;
                Console.AutoColorOutput = false;
                WriteLine(line, level, elements);
                Console.AutoColorOutput = autoColor;
                Console.ForeColor = fore;
            }
            else
                WriteLine(line, level, elements);
        }

        /// <summary>
        /// Writes the specified entry into all outputs with a higher or equal <see cref="LogLevel"/>, using the specified Font color.
        /// </summary>
        /// <param name="line">Entry that will be logged.</param>
        /// <param name="foreground">Font color used to display the message</param>
        /// <param name="level"><see cref="LogLevel"/> for the current entry.</param>
        /// <remarks>
        /// This method DO write an End Of Line to the outputs.
        /// </remarks>
        public static void WriteColoredLine(string line, ConsoleColor foreground, LogLevel level = LogLevel.Message)
        {
            WriteColoredLine(line, foreground, level, null);
        }

        /// <summary>
        /// Writes the specified entry into all outputs with a higher or equal <see cref="LogLevel"/>, using the specified Background and Font colors.
        /// </summary>
        /// <param name="line">Entry that will be logged.</param>
        /// <param name="background">Background color used to display the message</param>
        /// <param name="foreground">Font color used to display the message</param>
        /// <param name="level"><see cref="LogLevel"/> for the current entry.</param>
        /// <remarks>
        /// This method DO write an End Of Line to the outputs.
        /// </remarks>
        public static void WriteColoredLine(string line, ConsoleColor background, ConsoleColor foreground, LogLevel level = LogLevel.Message)
        {
            WriteColoredLine(line, background, foreground, level, null);
        }

        /// <summary>
        /// Writes a new line entry to the outputs with the specified severity, and generates a new Exception of the given type with the same message.
        /// </summary>
        /// <param name="message">Message logged to the outputs and stored into the generated Exception</param>
        /// <param name="severity">Severity for the logged message</param>
        /// <param name="excType">Base Exception type of the new generated Exception instance</param>
        /// <param name="exceptionArguments">Additional arguments passed to the Exception constructor method</param>
        /// <returns>The newly generated exception, ready to be thrown by the calling code</returns>
        /// <remarks>This method is designed to be used directly in a throw statement.</remarks>
        /// <example>throw WriteAndMakeException("Exception message goes here...", LogLevel.Info", typeof(ApplicationException), "Exception argument");</example>
        /// <exception cref="ArgumentException">The method defaults to this Exception when the <paramref name="excType"/> parameter is not a valid Exception type. In this case, the method will also append the condition to the message.</exception>
        public static Exception WriteAndMakeException(string message, LogLevel severity, Type excType,
            params object[] exceptionArguments)
        {
            if (excType == null || !excType.IsSubclassOf(typeof (Exception)))
            {
                message = message +
                          "\nAdditionally, com.RazorSoftware.Logging.Log.WriteAndMakeException was called with an invalid excType parameter. The method must be called with a valid Exception (or derived from) class.";
                excType = typeof(ArgumentException);
            }

            ConstructorInfo[] constructors = excType.GetConstructors();
            Exception outputException = null;

            foreach (ConstructorInfo ctor in constructors)
            {
                // Check if the constructor signature matches the specified parameters
                ParameterInfo[] parameters = ctor.GetParameters();

                if (parameters == null || parameters.Length < 1)
                    continue; // Ignore parameterless constructors

                if (!parameters[0].ParameterType.IsAssignableFrom(typeof (string)))
                    continue; // Ignore constructors whose first parameter is not assignable from string

                if (exceptionArguments == null || exceptionArguments.Length < 1)
                {
                    // Call with only a message since no additional arguments where specified
                    if (parameters.Length == 1)
                    {
                        // Valid method signature found. Call it
                        outputException = ctor.Invoke(new object[] {message}) as Exception;
                        break;
                    }
                }
                else
                {
                    // Call with a custom set of parameters
                    if (parameters.Length == 2)
                    {
                        // Check parameters[1] = exceptionArguments
                        if (parameters[1].ParameterType.IsInstanceOfType(exceptionArguments))
                        {
                            // Valid signature found, call it
                            outputException = ctor.Invoke(new object[] { message, exceptionArguments }) as Exception;
                            break;
                        }
                    }

                    if (parameters.Length == exceptionArguments.Length + 1)
                    {
                        // Find a method that matches the whole set of parameters
                        for (int i = 1; i < parameters.Length; i++)
                        {
                            if (!parameters[i].ParameterType.IsInstanceOfType(exceptionArguments[i-1]))
                                break; // Invalid Signature
                        }

                        // Valid signature found, call it.
                        object[] invocationParameters = new object[exceptionArguments.Length + 1];
                        Array.Copy(exceptionArguments, 0, invocationParameters, 1, invocationParameters.Length - 1);
                        invocationParameters[0] = message;

                        outputException = ctor.Invoke(invocationParameters) as Exception;
                    }
                }
            }

            if (outputException == null)
            {
                message = message +
                          "\nAdditionally, com.RazorSoftware.Logging.Log.WriteAndMakeException was unable to generate an appropriate exception from the given type.";
                outputException = new Exception(message);
            }

            WriteLine(message, severity);

            return outputException;
        }

        /// <summary>
        /// Writes information about the provided exception to the outputs
        /// </summary>
        /// <param name="exc">Target exception that will be logged to the outputs</param>
        /// <param name="severity">Severity for the logged message</param>
        /// <remarks>Exception information that will be printed includes: Exception Type (short name) and Exception Message</remarks>
        public static void WriteException(Exception exc, LogLevel severity)
        {
            if (exc == null)
                throw new ArgumentNullException("exc");

            StringBuilder str = new StringBuilder();
            str.Append("An exception has been generated. [");
            str.Append(exc.GetType().Name);
            str.Append("] ");
            str.Append(exc.Message);

            WriteLine(str.ToString(), severity);
        }

        /// <summary>
        /// Initializes this class and prepares some default outputs for entry logging.
        /// </summary>
        /// <exception cref="System.IO.IOException">Thrown when an IOException occurs on <see cref="OpenFile"/> while opening default outputs.</exception>
        /// <param name="openDefaultOutputs">Indicates if default outputs will be opened.</param>
        /// <remarks>
        /// If <paramref name="openDefaultOutputs"/> is not set, no output will be opened, but the Logger will still be initialized.
        /// <para></para>
        /// <para>These Standard Outputs differ from BuildType and are:</para>
        ///	<para>For Debug builds:</para>
        ///	<para>	A <see cref="ConsoleOutput"/> with <see cref="LogLevel.Debug"/></para>
        ///	<para>	A <see cref="DebuggerOutput"/> with <see cref="LogLevel.Warning"/></para>
        ///	<para>	A <see cref="FileOutput"/> with <see cref="LogLevel.Debug"/></para>
        ///	<para>	A <see cref="FileOutput"/> with <see cref="LogLevel.Message"/></para>
        ///	<para>For Release builds:</para>
        ///	<para>	A <see cref="FileOutput"/> with <see cref="LogLevel.Message"/></para>
        ///	<para>	A <see cref="DebuggerOutput"/> with <see cref="LogLevel.Error"/></para>
        /// </remarks>
        public static void Initialize(bool openDefaultOutputs)
        {
            // Create unique instances for Debugger and Console outputs
            Console = new ConsoleOutput();
            Debugger = new DebuggerOutput();

            // Initialize Objects
            _files = new List<FileOutput>();
            AccessLock = new Mutex();
            _customOutputs = new List<IOutput>();

            Errors = 0;
            Warnings = 0;
            Entries = 0;

            Enabled = true;

            Assembly ea = Assembly.GetEntryAssembly();
            if (String.IsNullOrEmpty(AppName))
            {
                if (ea != null)
                    AppName = ea.GetName().Name;
                else
                    AppName = "Debug Output";
            }

            if (String.IsNullOrEmpty(AppVersion))
            {
                if (ea != null)
                    AppVersion = ea.GetName().Version.ToString();
                else
                    AppVersion = "Unknown Version";
            }

            IsInitialized = true;

            if (!openDefaultOutputs)
                return;

#if DEBUG
            // Initialize DEBUG Output
            // Console output
            Console.MinimumLevel = LogLevel.Debug;
            Console.Enabled = true;
#if !NETCOREAPP
            if (!Console.HasConsole())
                Console.CreateConsole(AppName + " Debug output");
#endif
            // Debugger output
            Debugger.MinimumLevel = LogLevel.Warning;
            Debugger.Enabled = true;
            // File with Debug output
            OpenFile(".\\" + AppName + "-debugOutput.log", "debug", LogLevel.Debug, false);
            // File with Message output
            OpenFile(".\\" + AppName + ".log", "default", LogLevel.Message, false);
#else
            // Initialize RELEASE Output
            // Console output (disable if not already created)
            Console.MinimumLevel = LogLevel.Message;
#if !NETCOREAPP
            if (!Console.HasConsole())
                Console.Enabled = false;
#endif
            // Debugger output
            Debugger.MinimumLevel = LogLevel.Error;
            Debugger.Enabled = true;
            // File with Message output
            OpenFile(".\\" + AppName + ".log", "default", LogLevel.Message, false);
#endif
        }

        /// <summary>
        /// Initializes this class and prepares some default outputs for entry logging.
        /// </summary>
        /// <remarks>
        /// These Standard Outputs differ from BuildType and are:
        ///	<para>For Debug builds:</para>
        ///	<para>	A <see cref="ConsoleOutput"/> with <see cref="LogLevel.Debug"/></para>
        ///	<para>	A <see cref="DebuggerOutput"/> with <see cref="LogLevel.Warning"/></para>
        ///	<para>	A <see cref="FileOutput"/> with <see cref="LogLevel.Debug"/></para>
        ///	<para>	A <see cref="FileOutput"/> with <see cref="LogLevel.Message"/></para>
        ///	<para>For Release builds:</para>
        ///	<para>	A <see cref="FileOutput"/> with <see cref="LogLevel.Message"/></para>
        ///	<para>	A <see cref="DebuggerOutput"/> with <see cref="LogLevel.Error"/></para>
        /// </remarks>
        public static void Initialize()
        {
            Initialize(true);
        }

        /// <summary>
        /// Closes all outputs associated to this Log and frees any used resources
        /// </summary>
        /// <exception cref="ObjectDisposedException">Thrown when the instance has already shut down.</exception>
        public static void Shutdown()
        {
            Enabled = false;
            IsInitialized = false;

            if (AutoWrappingEnabled && _lastWriteLineStringRepeats > 0)
            {
                _pauseAutoWrapping = true;
                WriteLine(ExpandStrTokens(AutoWrappingReportString, _lastWriteLineStringRepeats.ToString("N0")), AutoWrappingReportLogLevel);
                _pauseAutoWrapping = false;
            }

            // Print statistics
            if (CloseStatistics)
                WriteLine("Logged %@ entries. %@ total Errors. %@ Warnings.", LogLevel.Message, Entries.ToString("N0"), Errors.ToString("N0"), Warnings.ToString("N0"));


            // Shutdown File Outputs
            if (_files.Count > 0)
            {
                try
                {
                    AcquireLock(_files);
                    foreach (FileOutput f in _files)
                        f.Close();
                }
                finally { ReleaseLock(_files); }

                _files.Clear();
            }

            // Shutdown Console Output
            AcquireLock(Console);
            Console.Enabled = false;
            Console.Dispose();
            ReleaseLock(Console);

            // Shutdown Debugger Output
            Debugger.Enabled = false;

            // Free Custom Objects
            _customOutputs.Clear();

            // Shutdown Log
            Console = null;
            Debugger = null;
            _files = null;
            AccessLock.Close();
            AccessLock = null;
            AppName = null;
            AppVersion = null;
        }

        /// <summary>
        /// Replaces all tokens in the specified string with the Description of the objects provided.
        /// </summary>
        /// <param name="entry">Source string with tokens for replace</param>
        /// <param name="elements">Array of objects that replaces the tokes on the entry parameter</param>
        /// <returns>An string with token information replaced with the objects on the elements array</returns>
        /// <remarks>If the <paramref name="elements"/> array is empty (no additional parameters were passed), the method will return the original entry unmodified.
        /// The first 15 Tokens can be referenced by its index in hexadecimal form (from 1 through F), but more tokens can be referenced by the positional auto-increment index (@).</remarks>
        public static string ExpandStrTokens(string entry, params object[] elements)
        {
            if (elements == null || elements.Length < 1 || string.IsNullOrEmpty(entry))
                return entry;

            StringBuilder str = new StringBuilder(1024);

            int segmentStart = 0;
            int elementPointer = 0;
            for (int i = 0; i < entry.Length; i++)
            {
                if (entry[i] == '%' && i < entry.Length - 1)
                {
                    if (segmentStart < i)
                        str.Append(entry.Substring(segmentStart, i - segmentStart));

                    i++;
                    char index = entry[i];

                    switch (index)
                    {
                        case '@':
                            if (elements[elementPointer] == null)
                                str.Append("NULL");
                            else
                                str.Append(elements[elementPointer].ToString());

                            elementPointer++;
                            break;

                        case '%':
                            str.Append('%');
                            break;

                        default:
                            if (char.IsDigit(index) || (char.ToLowerInvariant(index) >= 'a' && char.ToLowerInvariant(index) <= 'f'))
                            {
                                int val = int.Parse(index.ToString(CultureInfo.InvariantCulture), NumberStyles.AllowHexSpecifier, CultureInfo.InvariantCulture);
                                if (elements[val-1] == null)
                                    str.Append("NULL");
                                else
                                    str.Append(elements[val-1].ToString());
                            }
                            else
                            {
                                str.Append("%");
                                str.Append(index);
                            }
                            break;
                    }

                    segmentStart = i + 1;
                }
            }

            if (segmentStart < entry.Length)
                str.Append(entry.Substring(segmentStart));

            return str.ToString();
        }


        // Private methods
        /// <summary>
        /// Acquires a lock to a given object, using the options specified on the attributes of this instance.
        /// </summary>
        /// <exception cref="InvalidOperationException">Thrown when the lock cannot be acquired for the given object.</exception>
        /// <param name="obj">Object that will be locked for Single-Thread access</param>
        /// <remarks>
        /// If <see cref="SuppressSecurity"/> is set, then this methods will not lock objects and return immediately.
        /// <para>If <see cref="UseLocalSecurity"/> is set, then a Monitor object will be used to manage thread access to objects in this AppDomain, otherwise a Mutex object is used.</para>
        /// </remarks>
        private static void AcquireLock(object obj)
        {
            if (SuppressSecurity)
                return;

            try
            {
                if (UseLocalSecurity)
                    Monitor.Enter(obj);
                else
                    AccessLock.WaitOne();
            }
            catch (Exception ex)
            { throw new InvalidOperationException(ex.Message); }
        }
        /// <summary>
        /// Releases a lock previously acquired for the given object.
        /// </summary>
        /// <param name="obj">Object that will be released from the Lock</param>
        /// <remarks>
        /// If <see cref="SuppressSecurity"/> is set, then this methods will not lock objects and return immediately.
        /// <para>If <see cref="UseLocalSecurity"/> is set, then a Monitor object will be used to manage thread access to objects in this AppDomain, otherwise a Mutex object is used.</para>
        /// </remarks>
        private static void ReleaseLock(object obj)
        {
            if (SuppressSecurity)
                return;

            try
            {
                if (UseLocalSecurity)
                    Monitor.Exit(obj);
                else
                    AccessLock.ReleaseMutex();
            }
            catch (Exception ex)
            { throw new InvalidOperationException(ex.Message); }
        }
    }
}
