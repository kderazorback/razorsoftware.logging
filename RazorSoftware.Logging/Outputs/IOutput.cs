﻿/*
    Copyright © 2018 RazorSoftware
    Fabian R. kderazorback@me.com
    This file is part of RazorSoftware Logging Library.

    RazorSoftware Logging Library is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RazorSoftware Logging Library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RazorSoftware Logging Library.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;

namespace com.RazorSoftware.Logging.Outputs
{
    /// <summary>
    /// Allows Custom objects to be used as Outputs for entries sent to the <see cref="Log"/> Class.
    /// </summary>
#if RAZORSOFTWARE_PUBLIC_API
    public interface IOutput
#else
    internal interface IOutput
#endif
    {
        // Attributes
        /// <summary>
        /// Stores the identifier used as name and passed to this instance when created.
        /// </summary>
        string Identifier { get; }

        /// <summary>
        /// Stores the minimum <see cref="LogLevel"/> that will be logged by this instance.
        /// </summary>
        LogLevel MinimumLevel { get; set; }
        /// <summary>
        /// Specifies if this instance if enabled and will process Log entries.
        /// </summary>
        bool Enabled { get; set; }


        // Methods
        /// <summary>
        /// Writes the specified entry to the Output.
        /// </summary>
        /// <exception cref="InvalidOperationException">Thrown when the data cannot be written to the output.</exception>
        /// <param name="entry">Text that will be written to the Output</param>
        /// <remarks>
        /// This method DONT write an end of line to the output.
        /// </remarks>
        void Write(string entry);

        /// <summary>
        /// Writes the specified entry to the Output with the indicated severity.
        /// </summary>
        /// <exception cref="InvalidOperationException">Thrown when the data cannot be written to the output.</exception>
        /// <param name="entry">Text that will be written to the Output</param>
        /// <param name="severity">Severity for the logged message</param>
        /// <remarks>
        /// This method DONT write an end of line to the output.
        /// </remarks>
        void Write(string entry, LogLevel severity);

        /// <summary>
        /// Writes the specified entry to the Output.
        /// </summary>
        /// <exception cref="InvalidOperationException">Thrown when the data cannot be written to the output.</exception>
        /// <param name="entry">Text that will be written to the Output</param>
        /// <remarks>
        /// This method DO write an end of line to the output.
        /// </remarks>
        void WriteLine(string entry);

        /// <summary>
        /// Writes the specified entry to the Output with the indicated severity.
        /// </summary>
        /// <exception cref="InvalidOperationException">Thrown when the data cannot be written to the output.</exception>
        /// <param name="entry">Text that will be written to the Output</param>
        /// <param name="severity">Severity for the logged message</param>
        /// <remarks>
        /// This method DO write an end of line to the output.
        /// </remarks>
        void WriteLine(string entry, LogLevel severity);

        /// <summary>
        /// Writes an end of line to the output.
        /// <exception cref="InvalidOperationException">Thrown when the data cannot be written to the output.</exception>
        /// </summary>
        void WriteLine();

        /// <summary>
        /// Closes the Output.
        /// </summary>
        /// <exception cref="ObjectDisposedException">Thrown when the instance has already been shut down.</exception>
        void Close();
    }
}
